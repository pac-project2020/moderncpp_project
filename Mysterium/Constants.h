#pragma once
#include <iostream>

static const uint8_t noTypeCard = 18;

static std::pair<uint8_t, uint8_t> characterRange{ 1, 18 };
static std::pair<uint8_t, uint8_t> locationRange{ 19, 36 };
static std::pair<uint8_t, uint8_t> objectRange{ 37, 54 };

static const uint8_t noVisionCards = 112;

static const uint8_t ghostHand = 7;
static const uint8_t maxClarvoyanceTokens = 4;