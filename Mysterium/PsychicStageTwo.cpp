#include "Headers.h" // this has to be on the very first line !!!!

#include "PsychicStageTwo.h"
#include "Server.h"

PsychicStageTwo::PsychicStageTwo(std::shared_ptr<sf::RenderWindow> window) : Stage(std::move(window))
{
	voted = false;

	initializeText();
	initializeButtons();
	initializeBackground();
}

void PsychicStageTwo::update(const float& dt)
{
	if (gameOver)
		return;

	updateMousePosition();
	updateButtons();

	if (gameState == StageTwoState::Voting && !voted)
	{
		if (board->pickedCircumstance())
		{
			auto choice = board->getChoice();
			sendVote(choice);

			infoText.setString("Waiting for other psychics...");
			voted = true;
		}
	}
}

void PsychicStageTwo::render(sf::RenderTarget* target)
{
	if (!target)
		target = m_window.get();

	target->draw(m_background);

	// render circumstances
	board->render(target);

	// render vision cards
	if (visionCards.size())
	{
		for (auto& card : visionCards)
		{
			card->render(target);
		}
	}

	// render info text
	target->draw(infoText);
}

void PsychicStageTwo::changeGameState(StageTwoState state)
{
	gameState = state;

	switch (gameState)
	{
	case StageTwoState::PickingCircumstance:
	{
		infoText.setString("Waiting for Ghost...");
		break;
	}
	case StageTwoState::RecivingVisionCards:
	{
		infoText.setString("Waiting for Ghost...");
		break;
	}
	case StageTwoState::Voting:
	{
		infoText.setString("Pick a Circumstance");
		break;
	}
	case StageTwoState::Results:
	{
		infoText.setString("");
		break;
	}
	}
}

void PsychicStageTwo::receiveVisionCards(const std::vector<std::string>& cardNames)
{
	sf::Vector2i offset(200, 200);
	int padding = 220;

	int counter = 0;
	for (auto cardName : cardNames)
	{
		int x = offset.x + counter * padding;
		int y = offset.y;

		//Card* card = new Card(x, y, m_textures[cardName], cardName);
		auto card = std::make_unique<Card>(x, y, m_textures[cardName], cardName);
		card->setPosition(x, y);
		card->setScale(0.5, 0.5);

		visionCards.push_back(std::move(card));

		counter++;
	}
}

void PsychicStageTwo::initializeText()
{
	if (!font.loadFromFile("../External/Resources/Fonts/arial.ttf"))
	{
		throw "ERROR::STAGE::COULD_NOT_LOAD_FONT";
	}

	SetText(infoText, font, sf::Color::White, 30, sf::Vector2f(10.0f, 10.0f));
}

void PsychicStageTwo::initializeButtons()
{
	auto stringCircumstances = Server::getCircumstances();
	//board = new Board(m_textures, stringCircumstances);
	board = std::make_unique<Board>(m_textures, stringCircumstances);
	board->initializeCircumstancesAsButtons();
}

void PsychicStageTwo::initializeBackground()
{
	m_background.setSize(sf::Vector2f(m_window->getSize()));
	m_background.setTexture(&m_textures["BACKGROUND"]);
}

void PsychicStageTwo::updateButtons()
{
	if (gameState == StageTwoState::Voting && !voted)
		board->update(mousePosWindow);
}

void PsychicStageTwo::sendVote(Color circumstance)
{
	auto stage = std::dynamic_pointer_cast<ServerStageTwo>(Server::getCurrentStage());
	if (stage)
	{
		stage->receiveVote(circumstance);
	}
}
