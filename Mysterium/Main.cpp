#include "Headers.h" // this has to be on the very first line !!!!

#include "../Logging/Logging.h"
#include "Game.h"

int main()
{
	Game game;
	game.run();
	return 0;
}