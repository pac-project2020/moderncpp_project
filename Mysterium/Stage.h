#pragma once

#include "Button.h"


class Stage
{
public:
	Stage(std::shared_ptr<sf::RenderWindow> window);
	virtual void update(const float& dt) = 0;
	virtual void render(sf::RenderTarget* target = nullptr) = 0;
	virtual ~Stage();

	std::vector<std::string> getVisionCardNames();

	void setGameOver(bool won);

	static void initializeTextures();

protected:
	virtual void initializeButtons() = 0;
	virtual void initializeBackground() = 0;
	virtual void updateButtons() = 0;

	void SetText(sf::Text& text, const sf::Font& font, const sf::Color& fillColor, uint16_t size, const sf::Vector2f& position, float thickness = 0);
	void SetText(sf::Text& text, const sf::Font& font, const sf::Color& fillColor, uint16_t size, float thickness = 0);

	void updateMousePosition();
	void updateKeyTime(const float& dt);
	bool getKeyTime();

	void renderButtons(sf::RenderTarget& target);

private:
	static bool readCardNames(std::string fileName);
	static void readVisionCards();

protected:
	bool gameOver;

	std::shared_ptr<sf::RenderWindow> m_window;
	sf::RectangleShape m_background;

	sf::Vector2i mousePosScreen;
	sf::Vector2i mousePosWindow;

	std::map<std::string, std::unique_ptr<Button>> m_buttons;

	//Resources
	static std::map<std::string, sf::Texture> m_textures;

	static std::vector<std::string> characterCardNames;
	static std::vector<std::string> locationCardNames;
	static std::vector<std::string> objectCardNames;

	static std::vector<std::string> cardNames;
	static std::vector<std::string> visionCardNames;

	sf::Font font;
	sf::Text infoText;

	Logger logger;
private:
	float keyTime;
	float keyTimeMax;

};

