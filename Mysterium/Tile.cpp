#include "Headers.h" // this has to be on the very first line !!!!

#include "Tile.h"

Tile::Tile(int x, int y, const sf::Texture& texture)
{
	m_sprite.setTexture(texture);
	m_sprite.setPosition(sf::Vector2f((float)x,(float)y));
}

Tile::Tile(const sf::Texture& texture)
{
	m_sprite.setTexture(texture);
}

void Tile::setTexture(const sf::Texture& texture)
{
	m_sprite.setTexture(texture);
}

void Tile::setPosition(int x, int y)
{
	m_sprite.setPosition(sf::Vector2f((float)x, (float)y));
}

void Tile::setScale(float x, float y)
{
	m_sprite.setScale(sf::Vector2f(x, y));
}

sf::Vector2f Tile::getPosition()
{
	return m_sprite.getPosition();
}

sf::Vector2f Tile::getDimension()
{
	auto bounds = m_sprite.getGlobalBounds();
	return sf::Vector2f(bounds.height, bounds.width);
}

sf::Texture Tile::getTexture()
{
	return *m_sprite.getTexture();
}

void Tile::render(sf::RenderTarget* target)
{
	target->draw(m_sprite);
}
