#pragma once
#include "PsychicClient.h"
#include "Tile.h"

class Token : public Tile
{
private:
	Color m_color;

public:
	Token(const sf::Texture& texture, Color color);
	~Token() = default;

	Color getColor();
	sf::Vector2f getSpriteScale();
	sf::Vector2f getSpritePosition();
};