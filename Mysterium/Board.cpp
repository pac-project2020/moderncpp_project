#include "Headers.h" // this has to be on the very first line !!!!

#include "Board.h"
#include "PsychicClient.h"
#include "Stage.h"

Board::Board(std::map<std::string, sf::Texture>& textures, const std::map<Color, std::tuple<std::string, std::string, std::string>>& circumstances) : m_textures(textures)
{
	for (auto& c : circumstances)
	{
		Color psychic = c.first;
		std::string person = std::get<0>(c.second);
		std::string location = std::get<1>(c.second);
		std::string object = std::get<2>(c.second);

		psychicCircumstances[c.first] = std::make_unique<Circumstance>(
			std::make_shared<Tile>(m_textures[person]),
			std::make_shared<Tile>(m_textures[location]),
			std::make_shared<Tile>(m_textures[object])
		);

	}
	setCircumstancePositions();
}

void Board::update(std::vector<Color> correctlyPlacedTokens)
{
	for (auto& color : correctlyPlacedTokens)
	{
		psychicCircumstances[color]->flipNextCard();
	}
}

void Board::update(sf::Vector2i mousePos)
{
	for (auto& [color,circumstance] : psychicCircumstances)
		circumstance->update(mousePos);
}

void Board::render(sf::RenderTarget* target)
{
	for (auto& pair : psychicCircumstances)
	{
		//auto c = ;
		pair.second->render(target);
	}
}

bool Board::pickedCircumstance()
{
	for (auto it = psychicCircumstances.begin(); it != psychicCircumstances.end(); ++it)
	{
		//auto circumstance = (*it).second;
		if ((*it).second->isPressed())
		{
			return true;
		}
	}

	return false;
}

Color Board::getChoice()
{
	for (auto it = psychicCircumstances.begin(); it != psychicCircumstances.end(); ++it)
	{
		//auto circumstance = (*it).second;
		if ((*it).second->isPressed())
		{
			auto color = (*it).first;
			return color;
		}
	}

	throw "ERROR::BOARD::COULD_NOT_FIND_CHOICE";
}

void Board::clearUnusedCircumstances()
{
	auto it = psychicCircumstances.begin();
	while (it != psychicCircumstances.end()) 
	{
		//auto circumstance = (*it).second;
		if (!(*it).second->isPressed())
		{
			auto elementToErase = it;
			++it;

			//delete circumstance;
			psychicCircumstances.erase(elementToErase);
		}
		else 
		{
			++it;
		}
	}
}

void Board::initializeCircumstancesAsButtons()
{
	for (auto& [psychic,circumstance] : psychicCircumstances)
	{
		circumstance->initializeAsButton();
	}
}

void Board::setCircumstancePositions() {

	const sf::Vector2i offset(20, 600);
	const sf::Vector2i padding(430, 250);
	constexpr int spacer = 270;
	
	int contor = 0;
	//C++17
	for (auto const& [key, val] : psychicCircumstances)
	{
		int x = offset.x + contor * padding.x;
		int y = offset.y;
		//Circumstance* currentCircumstances = val;
		val->getCharacterCard()->setPosition(x, y);
		val->getLocationCard()->setPosition(x, y + padding.y);
		val->getObjectCard()->setPosition(x + spacer, y + padding.y);

		val->getCharacterCard()->setScale(0.4f, 0.4f);
		val->getLocationCard()->setScale(0.3f, 0.3f);
		val->getObjectCard()->setScale(0.3f, 0.3f);
		contor++;
	}

}
