#include "Headers.h" // this has to be on the very first line !!!!

#include "GraphicsSettings.h"

GraphicsSettings::GraphicsSettings() :
	m_title{ "Default" },
	m_resolution{ sf::VideoMode::getDesktopMode() },
	m_fullscreen{ false }
{
}

void GraphicsSettings::LoadFromFile(const std::string& path)
{
	std::ifstream in(path);
	if (in.is_open())
	{
		std::getline(in, m_title);
		in >> m_resolution.width
			>> m_resolution.height
			>> m_fullscreen;
	}

	in.close();
}
