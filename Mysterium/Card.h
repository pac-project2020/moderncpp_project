#pragma once
#include "Button.h"
#include "Token.h"
#include "Tile.h"

class Card : public Button, public Tile
{
private:
	std::string cardName;
	static sf::Texture back;

protected:
	std::vector<std::shared_ptr<Token>> tokens;

public:
	Card(const sf::Texture& front, std::string name); 
	Card(uint8_t x, uint8_t y, const sf::Texture& front, std::string name);
	Card(std::string name);
	Card(const sf::Texture& front);

	void insertToken(std::shared_ptr<Token> token);
	bool containsToken(Color color);
	std::shared_ptr<Token> removeToken(Color color);
	std::vector<std::shared_ptr<Token>>& getTokensVector();
	void clear();

	void setPosition(float x, float y);
	void setScale(float x, float y);
	std::string getName();

	void render(sf::RenderTarget* target);

	static sf::Texture& getBackTexture();
	static void setBackTexture(sf::Texture& texture);
};



