#include "Headers.h" // this has to be on the very first line !!!!

#include "Card.h"

sf::Texture Card::back;

Card::Card(const sf::Texture& front, std::string name) : Tile(front), Button(front.getSize().x, front.getSize().y)
{
	this->cardName = name;
	
}

Card::Card(uint8_t x, uint8_t y, const sf::Texture& front, std::string name) : Tile(x, y, front), Button(x, y, front.getSize().x, front.getSize().y), cardName(name)
{
}

Card::Card(std::string name)
{
	this->cardName = name;
}

Card::Card(const sf::Texture& front)
{
}

void Card::insertToken(std::shared_ptr<Token> token)
{
	// set the position of the token depending on its index inside the vector, and the coordinates of the card sprite

	sf::Vector2f origin = m_sprite.getPosition(); // the upper left corner of the card

	sf::Vector2i offset (10,10); // the offset from the origin
	int padding = 40; // the distance between 2 tokens

	int x = origin.x + offset.x + tokens.size() % 2 * padding;
	int y = origin.y + offset.y + tokens.size() / 2 * padding;

	token->setPosition(x, y);
	tokens.push_back(token);
}

bool Card::containsToken(Color color)
{
	for (auto token : tokens)
	{
		if (token->getColor() == color)
			return true;
	}

	return false;
}

std::shared_ptr<Token> Card::removeToken(Color color)
{
	for (auto it = tokens.begin(); it != tokens.end(); ++it)
	{
		auto token = *it;
		if (token->getColor() == color)
		{
			tokens.erase(it);
			return token;
		}
	}

	return nullptr;
}

std::vector<std::shared_ptr<Token>>& Card::getTokensVector()
{
	return tokens;
}

void Card::clear()
{
	tokens.clear();
}

void Card::setPosition(float x, float y)
{
	Button::setPosition(x, y);
	Tile::setPosition(x, y);
}

void Card::setScale(float x, float y)
{
	Button::setScale(x, y);
	Tile::setScale(x, y);
}

std::string Card::getName()
{
	return cardName;
}

void Card::render(sf::RenderTarget* target)
{
	Tile::render(target);
	//Button::render(target);

	if (tokens.size())
	{
		for (auto& token : tokens)
		{
			token->render(target);
		}
	}
}

sf::Texture& Card::getBackTexture()
{
	return back;
}

void Card::setBackTexture(sf::Texture& texture)
{
	back = texture;
}


