#pragma once
#include "GhostClient.h"
#include "PsychicClient.h"
#include "Constants.h"
#include "Stage.h"
#include "ServerStageOne.h"
#include "ServerStageTwo.h"
#include "../Logging/Logging.h"
#include <map>

class Server
{
	using Circumstance = std::tuple<std::string, std::string, std::string>;

public:
	static void initialize();

	static void initializePlayerNumber();
	static void initializeUnusedCards();
	static void readCircumstanceCardNames();
	static void initializeClients();
	static std::vector<Color>& getPsychicColors();

	static uint8_t getNumberOfPlayers();

	static void loadStageOne();
	static void loadStageTwo(const std::map<Color, uint8_t>& clarvoyancePoints);
	static void update();

	// client - server stuff
	static void connectAsPsychic(std::shared_ptr<PsychicClient> client);
	static void connectAsGhost(std::shared_ptr<GhostClient> client);

	static std::string requestVisionCard();
	static const std::map<Color, Circumstance>& getCircumstances();
	static const std::vector<std::string> getCheckpointCards(uint8_t checkpoint);

	static std::shared_ptr<ServerStage> getCurrentStage();

private:
	static uint8_t getRandomInt(uint8_t min, uint8_t max);
	static void generateCircumstances();
	static const std::vector<std::string> generateCheckpointcards(uint8_t checkpoint);
	static std::string pickRandomCircumstanceCard(std::vector<std::string> cards);


private:
	static const uint8_t maxPlayers = 5;

	static uint8_t numberOfPlayers; //initializat cu 0, se incrementeaza de fiecare data cand cineva se conecteaza

	static std::shared_ptr<ServerStage> currentStage;

	static std::vector<std::shared_ptr<PsychicClient>> psychicClients;
	static std::shared_ptr<GhostClient> ghostClient;
	static std::vector<std::string> unusedCards;
	static std::map<Color, Circumstance> psychicCircumstances;

	static std::vector<std::string> characterCardNames;
	static std::vector<std::string> locationCardNames;
	static std::vector<std::string> objectCardNames;

	static Logger logger;
};

