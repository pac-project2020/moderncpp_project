#include "Headers.h" // this has to be on the very first line !!!!

#include "PsychicStageOne.h"
#include "Server.h"

PsychicStageOne::PsychicStageOne(std::shared_ptr<sf::RenderWindow> window, Color color) : Stage(std::move(window)), psychicColor(color)
{
	currentCheckpoint = 0;
	guessedAllCards = false;

	initializeButtons();
	initializeBackground();
	initializeText();
}

PsychicStageOne::~PsychicStageOne()
{
}

void PsychicStageOne::update(const float& dt)
{
	if (gameOver)
		return;

	updateMousePosition();
	updateButtons();

	switch (gameState)
	{
	case StageOneState::Voting:
	{
		if (!pickedCard && !guessedAllCards)
		{
			// check player choice
			auto playerChoice = checkpoints.at(currentCheckpoint)->getPlayerChoice();

			if (playerChoice)
			{
				auto stage = std::dynamic_pointer_cast<ServerStageOne>(Server::getCurrentStage());
				if (stage)
				{
					stage->placeToken(psychicColor, playerChoice->getName());
					pickedCard = true;

					updateInfoText();
				}
			}
		}

		break;
	}
	case StageOneState::ClairevoyanceVoting:
	{
		if (!votedForClarvoyance) // if the psychic hasn't sent the votes yet
		{
			if (clarvoyanceTokens > 0 && currentPsychicColor < Server::getNumberOfPlayers() - 1) // if it can vote, check for button input
			{
				updateKeyTime(dt);

				if (getKeyTime())
				{
					if (clarvoyanceButtons["BUTTON_CORRECT"]->isPressed())
					{
						clarvoyanceVotes.push_back(std::make_tuple((Color)currentPsychicColor, true));
						updateCurrentPsychicColor();
						--clarvoyanceTokens;
						updateClarvoyanceText();

						logger.Log(Logger::Level::Info, "The psychic was correct voted!");

						updateInfoText();

						clarvoyanceButtons["BUTTON_CORRECT"]->reset();
						return;
					}

					if (clarvoyanceButtons["BUTTON_INCORRECT"]->isPressed())
					{
						clarvoyanceVotes.push_back(std::make_tuple((Color)currentPsychicColor, false));
						updateCurrentPsychicColor();
						--clarvoyanceTokens;
						updateClarvoyanceText();

						logger.Log(Logger::Level::Info, "The psychic wasn't correct voted!");

						updateInfoText();

						clarvoyanceButtons["BUTTON_INCORRECT"]->reset();
						return;
					}

					if (clarvoyanceButtons["BUTTON_SKIP"]->isPressed())
					{
						updateCurrentPsychicColor();

						updateInfoText();
						logger.Log(Logger::Level::Info, "The psychic was pressed the button : SKIP !");

						clarvoyanceButtons["BUTTON_SKIP"]->reset();
						return;
					}
				}
			}
			else
			{
				sendClarvoyanceVotes(); // else send the votes as they are
				updateInfoText();
			}
		}

		break;
	}
	}
}

void PsychicStageOne::render(sf::RenderTarget* target)
{
	if (!target)
		target = m_window.get();

	target->draw(m_background);

	for (auto& checkpoint : checkpoints)
		checkpoint->render(target);

	// draw the vision cards
	if (gameState == StageOneState::ClairevoyanceVoting && !votedForClarvoyance) // if voting for clarvoyance, draw the voted psychic's cards
		for (auto& card : visionCards[(Color)currentPsychicColor])
		{
			card->render(target);
		}
	else
		for (auto& card : visionCards[psychicColor]) // else draw this psychic's cards
		{
			card->render(target);
		}


	// draw a text that shows the current turn

	m_window->draw(currentTurn);

	target->draw(infoText);
	target->draw(clarvoyanceTokensText);

	if (gameState == StageOneState::ClairevoyanceVoting && !votedForClarvoyance)
	{
		for (auto& [name, button] : clarvoyanceButtons)
			button->render(target);
	}
}

void PsychicStageOne::initializeText()
{
	if (!font.loadFromFile("../External/Resources/Fonts/arial.ttf"))
	{
		throw "ERROR::STAGE::COULD_NOT_LOAD_FONT";
	}

	SetText(infoText, font, sf::Color::White, 30, sf::Vector2f(10.0f, 10.0f));
	SetText(currentTurn, font, sf::Color::White, 30, sf::Vector2f(1700.0f, 10.0f));
	SetText(clarvoyanceTokensText, font, sf::Color::White, 30, sf::Vector2f(1600.0f, 40.0f));

	currentTurn.setString("Current turn: 1");
	clarvoyanceTokensText.setString("Clarvoyance tokens: 4");
}

void PsychicStageOne::initializeBackground()
{
	m_background.setSize(sf::Vector2f(m_window->getSize()));
	m_background.setTexture(&m_textures["BACKGROUND"]);
}

void PsychicStageOne::initializeButtons()
{
	sf::Vector2f offset(1200, 100);
	int padding = 100;

	clarvoyanceButtons["BUTTON_CORRECT"] = std::make_unique<Button>(offset.x, offset.y, &m_textures["BUTTON_CORRECT"]);
	clarvoyanceButtons["BUTTON_INCORRECT"] = std::make_unique<Button>(offset.x, offset.y + padding, &m_textures["BUTTON_INCORRECT"]);
	clarvoyanceButtons["BUTTON_SKIP"] = std::make_unique<Button>(offset.x, offset.y + padding * 2, &m_textures["BUTTON_SKIP"]);
}

void PsychicStageOne::initializeCheckpoints()
{
	sf::Vector2i offset(20, 950); // coltul din stanga jos, pentru ca se incepe de jos in sus
	int padding = 300; // distanta intre 2 checkpoint-uri

	// first 3 checkpoints
	for (uint8_t i = 0; i < 3; ++i)
	{
		auto cardNames = Server::getCheckpointCards(i);

		std::vector<std::shared_ptr<Card>> cards;
		for (auto& name : cardNames)
		{
			auto card = std::make_shared<Card>(m_textures[name], name);
			card->setScale(0.3f, 0.3f);

			cards.push_back(card);
		}

		int x = offset.x;
		int y = offset.y - i * padding;
		checkpoints.push_back(std::make_unique<Checkpoint>(x, y, m_textures["CHECKPOINT"], cards));
		//checkpoints.push_back(std::make_unique<Checkpoint>(x, y, m_textures["CHECKPOINT"], cards));
	}

	// the last checkpoint(no cards)

	checkpoints.push_back(std::make_unique<Checkpoint>(offset.x, offset.y - 3 * padding, m_textures["CHECKPOINT"]));
	//checkpoints.push_back(std::make_unique<Checkpoint>(offset.x, offset.y - 3 * padding, m_textures["CHECKPOINT"]));

	// initialize tokens

	for (uint8_t i = 0; i < Server::getNumberOfPlayers() - 1; ++i)
	{
		std::stringstream ss;
		Color c = (Color)i;

		std::string colorToString = PsychicClient::colorToString[c];
		std::transform(colorToString.begin(), colorToString.end(), colorToString.begin(), ::toupper);

		ss << "TOKEN_" << colorToString;

		checkpoints[0]->insertToken(std::make_shared<Token>(m_textures[ss.str()], c)); /////////////////////////////////////
	}
}

void PsychicStageOne::changeGameState(StageOneState state)
{
	gameState = state;

	switch (gameState)
	{
	case StageOneState::Voting:
	{
		pickedCard = false;

		break;
	}
	case StageOneState::ClairevoyanceVoting:
	{
		if (psychicColor == (Color)0)
			currentPsychicColor = 1;
		else
			currentPsychicColor = 0;

		votedForClarvoyance = false;
		clarvoyanceVotes.clear();

		break;
	}
	}

	updateInfoText();
}

void PsychicStageOne::recieveTokenResults(const std::vector<Color>& results)
{
	// for each checkpoint (from the second-last to the first, so that advancing happens only once)
	//		if that checkpoints contains any of the colors in the given vector
	//			remove the token with the corresponding color and insert it into the next checkpoint
	//		reset the checkpoint so that the remaining tokens aren't standing on the cards

	for (int i = checkpoints.size() - 2; i >= 0; --i)
	{
		for (auto& color : results)
		{
			if (checkpoints[i]->containsOnCard(color))
			{
				auto token = checkpoints[i]->removeToken(color); ///////////////////////////////
				checkpoints[i + 1]->insertToken(token);

				if (color == psychicColor)
				{
					currentCheckpoint = i + 1;

					if (currentCheckpoint == checkpoints.size() - 1)
						guessedAllCards = true;
				}
			}
		}

		checkpoints[i]->resetCardTokens();
	}
}

void PsychicStageOne::recieveVisionCard(Color color, std::string cardName)
{
	sf::Vector2i offset(1200, 500);
	sf::Vector2i padding(180, 260);

	auto cards = visionCards[color];

	int x = offset.x + visionCards[color].size() % 4 * padding.x;
	int y = offset.y + (visionCards[color].size() / 4) * padding.y;

	auto card = std::make_shared<Tile>(x, y, m_textures[cardName]);
	card->setPosition(x, y);
	card->setScale(0.4f, 0.4f);

	visionCards[color].push_back(std::move(card));
}

void PsychicStageOne::recieveTokenPlacement(Color color, std::string card)
{
	for (auto& checkpoint : checkpoints)
	{
		if (checkpoint->contains(color))
		{
			checkpoint->moveToken(color, card);
			break;
		}
	}
}

void PsychicStageOne::setClarvoyanceTokens(uint8_t number)
{
	clarvoyanceTokens = number;
}

void PsychicStageOne::updateTurn(uint8_t turn)
{
	std::stringstream ss;
	ss << "Current turn: " << std::to_string(turn);

	currentTurn.setString(ss.str());
	logger.Log(Logger::Level::Info, "Current turn: " + std::to_string(turn));
}

void PsychicStageOne::updateButtons()
{
	if (gameState == StageOneState::ClairevoyanceVoting)
	{
		for (auto& [name, button] : clarvoyanceButtons)
		{
			button->update(mousePosWindow);
		}
	}

	if (gameState == StageOneState::Voting)
	{
		checkpoints[currentCheckpoint]->update(mousePosWindow);
	}
}

void PsychicStageOne::updateInfoText()
{
	switch (gameState)
	{
	case StageOneState::ReceivingVisionCards:
	{
		logger.Log(Logger::Level::Info, "Current stage: ReceivingVisionCards");

		infoText.setString("Waiting for Ghost...");
		break;
	}
	case StageOneState::Voting:
	{
		logger.Log(Logger::Level::Info, "Current stage: Voting");

		if (!pickedCard && !guessedAllCards)
			infoText.setString("Pick a card");
		else
			infoText.setString("Waiting for other psychics...");

		break;
	}
	case StageOneState::ClairevoyanceVoting:
	{
		logger.Log(Logger::Level::Info, "Current stage: ClairevoyanceVoting");

		if (!votedForClarvoyance)
		{
			std::stringstream ss;
			ss << "Vote for " << PsychicClient::colorToString[(Color)currentPsychicColor];
			infoText.setString(ss.str());
		}
		else
			infoText.setString("Waiting for other psychics' clarvoyance votes...");

		break;
	}
	case StageOneState::Results:
	{
		infoText.setString("");

		break;
	}
	}
}

void PsychicStageOne::updateClarvoyanceText()
{
	std::stringstream ss;
	ss << "Clarvoyance tokens: " << std::to_string(clarvoyanceTokens);

	clarvoyanceTokensText.setString(ss.str());
}

void PsychicStageOne::sendClarvoyanceVotes()
{
	auto stage = std::dynamic_pointer_cast<ServerStageOne>(Server::getCurrentStage());
	if (stage)
	{
		if (clarvoyanceVotes.size())
		{
			stage->voteForClarvoyance(psychicColor, clarvoyanceVotes);
		}
		else
			stage->skipClarvoyanceVote();

		votedForClarvoyance = true;
	}
}

void PsychicStageOne::updateCurrentPsychicColor()
{
	++currentPsychicColor;
	if ((Color)currentPsychicColor == psychicColor)
		++currentPsychicColor;
}
