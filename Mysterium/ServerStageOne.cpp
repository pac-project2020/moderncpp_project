#include "Headers.h" // this has to be on the very first line !!!!

#include "ServerStageOne.h"
#include "PsychicStageOne.h"
#include "GhostStageOne.h"
#include"Server.h"

ServerStageOne::ServerStageOne(std::vector<std::shared_ptr<PsychicClient>>& pClients, std::shared_ptr<GhostClient> gClient, std::map<Color, Circumstance>& psychicCircumstances)
	: ServerStage(pClients, std::move(gClient)), circumstances(psychicCircumstances)
{
	turn = 1;
	correctGuesses = 0;

	gameState = StageOneState::ReceivingVisionCards;
}

bool ServerStageOne::checkForEndStage()
{
	// check if stage one is done (max turn OR all psychics guessed all 3 cards)

	return turn == maxTurns || correctGuesses == circumstances.size();
}

void ServerStageOne::updateTurn()
{
	this->turn++;

	for (auto& psychicClient : psychicClients)
	{
		auto stage = std::dynamic_pointer_cast<PsychicStageOne>(psychicClient->getCurrentStage());
		if (stage)
			stage->updateTurn(turn);
	}

	auto stage = std::dynamic_pointer_cast<GhostStageOne>(ghostClient->getCurrentStage());
	if (stage)
		stage->updateTurn(turn);
}

void ServerStageOne::handleStageEnd() //last turn, everyone guessed or not
{
	// if all psychics guessed correctly
	//		tell all clients to load stage 2
	//		send  clarvoyance points
	// else
	//		game over

	if (correctGuesses == Server::getNumberOfPlayers() - 1)
	{
		// load stage 2
		Server::loadStageTwo(clarvoyancePoints);
	}
	else
	{
		// game over
		gameOver = true;

		for (auto& client : psychicClients)
		{
			auto stage = client->getCurrentStage();
			if (stage)
				stage->setGameOver(false);
		}

		auto stage = ghostClient->getCurrentStage();
		if (stage)
			stage->setGameOver(false);
	}
}

void ServerStageOne::update()
{
	if (gameOver)
		return;

	switch (gameState)
	{
	case StageOneState::ReceivingVisionCards:
	{
		if (!waitingForClients())
		{
			// reset the counter and change the state
			this->actionsPerformed = 0;
			this->gameState = StageOneState::Voting;
			signalGameStateChange();
		}
		// else do nothing untill the ghost gives a vision card to every psychic

		break;
	}

	case StageOneState::Voting:
	{
		if (!waitingForClients())
		{
			calculateTokenResults();
			tokenPositions.clear();

			// reset the counter and change the state
			this->actionsPerformed = 0;
			this->gameState = StageOneState::ClairevoyanceVoting;
			signalGameStateChange();
		}
		// else do nothing until every psychic places their token on a card

		break;
	}
	case StageOneState::ClairevoyanceVoting:
	{
		if (!waitingForClients())
		{
			// reset the counter and change the state
			this->actionsPerformed = 0;
			this->gameState = StageOneState::Results;
		}
		// else do nothing until every psychic votes (skipping counts too)

		break;
	}
	case StageOneState::Results:
	{
		// send the token placing results to all clients
		sendTokenResults();

		if (checkForEndStage())
		{
			handleStageEnd();

			return;
		}

		updateTurn();


		if (turn == 4)
		{
			for (auto& psychic : psychicClients)
			{
				auto stage = std::dynamic_pointer_cast<PsychicStageOne>(psychic->getCurrentStage());
				if (stage)
					stage->setClarvoyanceTokens(maxClarvoyanceTokens);
			}
		}

		// reset the counter and change the state
		this->gameState = StageOneState::ReceivingVisionCards;
		signalGameStateChange();

		break;
	}
	}
}

void ServerStageOne::startStageOne()
{
	signalGameStateChange();
}

void ServerStageOne::giveVisionCard(Color psychic, std::string card)
{
	for (auto& psychicClient : psychicClients)
	{
		auto stage = std::dynamic_pointer_cast<PsychicStageOne>(psychicClient->getCurrentStage());
		if (stage)
			stage->recieveVisionCard(psychic, card);
	}

	actionsPerformed++;
}

void ServerStageOne::voteForClarvoyance(Color psychic, std::vector<Vote> votes)
{
	++actionsPerformed;

	// for each vote (PsychicClient::Color, bool), if the vote is correct, increase the clarvoyance points

	for (auto& [otherPsychic, vote] : votes)
	{
		bool isCorrectPlacement = std::find(correctlyPlacedTokens.begin(), correctlyPlacedTokens.end(), otherPsychic) != correctlyPlacedTokens.end();

		if (isCorrectPlacement == vote)
			++clarvoyancePoints[psychic];
	}
}

void ServerStageOne::skipClarvoyanceVote()
{
	++actionsPerformed;
}

void ServerStageOne::placeToken(Color psychic, std::string card)
{
	tokenPositions[psychic] = card;
	++actionsPerformed;

	for (auto& psychicClient : psychicClients)
	{
		auto stage = std::dynamic_pointer_cast<PsychicStageOne>(psychicClient->getCurrentStage());
		if (stage)
		{
			stage->recieveTokenPlacement(psychic, card);
		}
	}
}

bool ServerStageOne::waitingForClients()
{
	return this->actionsPerformed < Server::getNumberOfPlayers() - 1 - correctGuesses;
}

void ServerStageOne::signalGameStateChange()
{
	for (auto& client : psychicClients)
	{
		auto stage = std::dynamic_pointer_cast<PsychicStageOne>(client->getCurrentStage());
		if (stage)
			stage->changeGameState(gameState);
	}

	auto stage = std::dynamic_pointer_cast<GhostStageOne>(ghostClient->getCurrentStage());
	if (stage)
		stage->changeGameState(gameState);
}

void ServerStageOne::sendTokenResults()
{
	for (auto& client : psychicClients)
	{
		auto stage = std::dynamic_pointer_cast<PsychicStageOne>(client->getCurrentStage());
		if (stage)
			stage->recieveTokenResults(correctlyPlacedTokens);
	}

	auto stage = std::dynamic_pointer_cast<GhostStageOne>(ghostClient->getCurrentStage());
	if (stage)
		stage->recieveTokenResults(correctlyPlacedTokens);
}

void ServerStageOne::sendClarvoyancePoints()
{
	// TO DO: tell every psychic how many clarvoyance points they have
}

void ServerStageOne::calculateTokenResults()
{
	// checks to see who placed their tokens on the right card (using the tokenPositions and circumstances containers)
	// and puts the correctly placed tokens inside the correctlyPlacedTokens vector

	correctlyPlacedTokens.clear();

	for (auto const& [psychic,tokenPosition] : tokenPositions)
	{
		auto circumstance = circumstances[psychic];

		if (std::get<0>(circumstance) == tokenPosition || std::get<1>(circumstance) == tokenPosition || std::get<2>(circumstance) == tokenPosition)
		{
			correctlyPlacedTokens.push_back(psychic);
			++checkpoints[psychic];

			if (checkpoints[psychic] == 3)
				++correctGuesses;
		}
	}
}
