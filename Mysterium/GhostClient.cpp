#include "Headers.h" // this has to be on the very first line !!!!

#include "GhostClient.h"
#include "GhostStageOne.h"
#include "GhostStageTwo.h"

GhostClient::GhostClient(std::shared_ptr<sf::RenderWindow> window) : Client(std::move(window))
{
	loadStageOne();
}

void GhostClient::loadStageOne()
{
	if (m_currentStage)
		m_currentStage.reset();

	m_currentStage = std::make_shared<GhostStageOne>(m_window, Server::getPsychicColors());
}

void GhostClient::loadStageTwo()
{
	if (m_currentStage)
		m_currentStage.reset();

	m_currentStage = std::make_shared<GhostStageTwo>(m_window);
}


