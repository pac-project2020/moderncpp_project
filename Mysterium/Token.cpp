#include "Headers.h" // this has to be on the very first line !!!!

#include "Token.h"

Token::Token(const sf::Texture& texture, Color color) : Tile(texture)
{
	m_color = color;
}

Color Token::getColor()
{
	return m_color;
}

sf::Vector2f Token::getSpritePosition()
{
	return m_sprite.getPosition();
}

sf::Vector2f Token::getSpriteScale()
{
	return m_sprite.getScale();
}

