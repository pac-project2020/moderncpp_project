#pragma once

#include "Client.h"

class GhostClient : public Client
{
public:
	GhostClient(std::shared_ptr<sf::RenderWindow> window);

	void loadStageOne() override;
	void loadStageTwo() override;
};

