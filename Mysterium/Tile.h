#pragma once
class Tile
{
protected:
	sf::Sprite m_sprite;

public:
	Tile() = default;
	Tile(int x, int y, const sf::Texture& texture);
	Tile(const sf::Texture& texture);
	~Tile() = default;

	void setTexture(const sf::Texture& texture);
	void setPosition(int x, int y);
	void setScale(float x, float y);

	sf::Vector2f getPosition();
	sf::Vector2f getDimension();
	sf::Texture getTexture();

	void render(sf::RenderTarget* target);
};

