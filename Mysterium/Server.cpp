#include "Headers.h" // this has to be on the very first line !!!!

#include "Server.h"
#include "Constants.h"
#include "PsychicStageOne.h"

std::random_device randomDevice;
std::mt19937 randomEngine(randomDevice());

using Circumstance = std::tuple<std::string, std::string, std::string>;

// membri statici

uint8_t Server::numberOfPlayers = 0; //initializat cu 0
std::shared_ptr<ServerStage> Server::currentStage;

std::vector<std::shared_ptr<PsychicClient>> Server::psychicClients;
std::vector<std::string> Server::unusedCards;
std::shared_ptr<GhostClient> Server::ghostClient;
std::map<Color, Circumstance> Server::psychicCircumstances;

std::vector<std::string> Server::characterCardNames;
std::vector<std::string> Server::locationCardNames;
std::vector<std::string> Server::objectCardNames;

// functii


void Server::initialize() 
{
	initializeUnusedCards();
	readCircumstanceCardNames();
}

void Server::initializePlayerNumber()
{
	bool readInput = true;
	unsigned int input;

	while (readInput)
	{
		std::cout << "Player number: ";
		std::cin >> input;

		if (input <= maxPlayers && input > 1)
		{
			readInput = false;
		}
		else if (input <= 1)
		{
			std::cout << "Player number must be at least 2." << std::endl;

			logger.Log(Logger::Level::Warning, "Only one player! Please try again!");
		}
		else
		{
			std::cout << "Player number must be less or equal to " << +maxPlayers << "." << std::endl;

			logger.Log(Logger::Level::Warning, "Too much players! Please try again!");
		}
	}

	numberOfPlayers = input;
	logger.Log(Logger::Level::Info, "Number of players is: " + std::to_string(numberOfPlayers));
}

void Server::initializeUnusedCards()
{
	std::stringstream ss;

	for (int i = 1; i <= noVisionCards; ++i)
	{
		ss << "visionCard (" << i << ").jpg";
		unusedCards.push_back(ss.str());
		ss.str(" ");
	}
}

void Server::readCircumstanceCardNames()
{
	std::ifstream in("../External/Resources/Cards/cardNames.txt");

	if (in.is_open())
	{
		for (int index1 = 0; index1 < characterRange.second; index1++) {
			std::string card;
			in >> card;
			characterCardNames.push_back(card);
		}
		for (int index2 = locationRange.first - 1; index2 < locationRange.second - 1; index2++) {
			std::string card;
			in >> card;
			locationCardNames.push_back(card);
		}
		for (int index3 = objectRange.first - 1; index3 < objectRange.second - 1; index3++) {
			std::string card;
			in >> card;
			objectCardNames.push_back(card);
		}

		in.close();
	}
	else
	{
		throw "ERROR::SERVER::FILE_NOT_FOUND";
	}
}

void Server::initializeClients()
{
	for (auto& psychic : psychicClients)
	{
		psychic->loadStageOne();
		auto stage = std::dynamic_pointer_cast<PsychicStageOne>(psychic->getCurrentStage());
		if (stage)
		{
			stage->initializeCheckpoints();
			stage->setClarvoyanceTokens(maxClarvoyanceTokens);
		}
	}
}

std::vector<Color>& Server::getPsychicColors()
{
	static std::vector<Color> colors;

	if (colors.size() == 0)
	{
		for (auto& psychic : psychicClients)
		{
			colors.push_back(psychic->getColor());
		}
	}

	return colors;
}

void Server::connectAsPsychic(std::shared_ptr<PsychicClient> client)
{
	static uint8_t color = 0;
	psychicClients.push_back(client);
	client->setColor(color++);
}

void Server::connectAsGhost(std::shared_ptr<GhostClient> client)
{
	ghostClient = std::move(client);
}

uint8_t Server::getNumberOfPlayers()
{
	return numberOfPlayers;
}

const std::vector<std::string> Server::getCheckpointCards(uint8_t checkpoint)
{
	static std::map<uint8_t, std::vector<std::string>> checkpointCards;
	if (checkpointCards[checkpoint].size() == 0)
		checkpointCards[checkpoint] = generateCheckpointcards(checkpoint);

	return checkpointCards[checkpoint];
}

std::shared_ptr<ServerStage> Server::getCurrentStage()
{
	return currentStage;
}

void Server::update()
{
	currentStage->update();
}

std::string Server::requestVisionCard()
{
	uint8_t cardIndex = getRandomInt(0, unusedCards.size() - 1);
	std::string cardName = unusedCards[cardIndex];
	unusedCards.erase(unusedCards.begin() + cardIndex);
	logger.Log(Logger::Level::Info, "A vision card was requested! ");
	return cardName;
}

const std::map<Color, Circumstance>& Server::getCircumstances()
{
	if (psychicCircumstances.size() == 0)
		generateCircumstances();

	return psychicCircumstances;
}

uint8_t Server::getRandomInt(uint8_t min, uint8_t max)
{
	std::uniform_int_distribution<int> unifDist(min, max);
	return unifDist(randomEngine);
}

void Server::generateCircumstances()
{
	for (auto& psychic : psychicClients)
	{
		std::string person = pickRandomCircumstanceCard(characterCardNames);
		std::string location = pickRandomCircumstanceCard(locationCardNames);
		std::string object = pickRandomCircumstanceCard(objectCardNames);

		psychicCircumstances[psychic->getColor()] = std::make_tuple(person, location, object);
	}
}

const std::vector<std::string> Server::generateCheckpointcards(uint8_t checkpoint)
{
	std::vector<std::string> cardNames;

	for (auto& pair : psychicCircumstances)
	{
		auto circumstance = pair.second;
		
		switch (checkpoint)
		{
		case 0:
		{
			cardNames.push_back(std::get<0>(circumstance));
			break;
		}
		case 1:
		{
			cardNames.push_back(std::get<1>(circumstance));
			break;
		}
		case 2:
		{
			cardNames.push_back(std::get<2>(circumstance));
			break;
		}
		default:
			throw "SERVER::INVALID_CHECKPOINT_VALUE";
		}
	}

	return std::move(cardNames);
}

std::string Server::pickRandomCircumstanceCard(std::vector<std::string> cards)
{
	uint8_t index = getRandomInt(0, cards.size() - 1);
	std::string card = cards[index];
	cards.erase(cards.begin() + index);

	return card;
}

void Server::loadStageOne()
{
	if (currentStage)
		currentStage.reset();

	currentStage = std::make_shared<ServerStageOne>(psychicClients, ghostClient, psychicCircumstances);

	std::dynamic_pointer_cast<ServerStageOne>(currentStage)->startStageOne();
}

void Server::loadStageTwo(const std::map<Color, uint8_t>& clarvoyancePoints)
{
	std::map<Color, uint8_t> cPoints = std::map(clarvoyancePoints);

	if (currentStage)
		currentStage.reset();

	for (auto& psychic : psychicClients)
	{
		psychic->loadStageTwo();
	}

	ghostClient->loadStageTwo();

	currentStage = std::make_shared<ServerStageTwo>(psychicClients, ghostClient, cPoints);
	logger.Log(Logger::Level::Info, "Loaded stage two!" + std::to_string(numberOfPlayers));
	
}

