#include "Headers.h" // this has to be on the very first line !!!!

#include "Game.h"
#include "Server.h"

Game::Game()
{
	logger.Log(Logger::Level::Info, "Started Mysterium!");
	initializeGraphics();
	initializeVariables();
	initializeServer();
	initializeMainMenu();
	
}

Game::~Game()
{
	
}

void Game::initializeServer()
{
	Server::initialize();
	Server::initializePlayerNumber();
	//createClients(Server::getNumberOfPlayers());
	//Server::loadStageOne();
}
void Game::initializeMainMenu()
{
	sf::RenderWindow window(sf::VideoMode(1371, 1106), "Mysterium");
	sf::Texture image;
	image.loadFromFile("../External/Resources/Background/bg2.jpg");
	sf::Sprite sprite(image);
	sf::Vector2u TextureSize;
	sf::Vector2u WindowSize;

	logger.Log(Logger::Level::Info, "Show entry menu!");
	TextureSize = image.getSize();
	WindowSize = window.getSize();

	float ScaleX = (float)WindowSize.x / TextureSize.x;
	float ScaleY = (float)WindowSize.y / TextureSize.y;

	sprite.setTexture(image);
	sprite.setScale(ScaleX, ScaleY);
	sf::Texture playButtonTextures;
	sf::Texture exitButtonTextures;
	playButtonTextures.loadFromFile("../External/Resources/Buttons/PLAY.png");
	exitButtonTextures.loadFromFile("../External/Resources/Buttons/EXIT.png");
	sf::Sprite playButton(playButtonTextures);
	sf::Sprite exitButton(exitButtonTextures);

	playButton.setPosition(sf::Vector2f(550.f, 500.f));
	exitButton.setPosition(sf::Vector2f(550.f, 600.f));



	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();


			if (event.mouseButton.button == sf::Mouse::Left)
			{
				sf::Vector2i MousePos = sf::Mouse::getPosition(window);
				sf::FloatRect playButtonPos = playButton.getGlobalBounds();
				sf::FloatRect exitButtonPos = exitButton.getGlobalBounds();

				if (playButtonPos.contains(MousePos.x, MousePos.y))
				{
					window.close();
					createClients(Server::getNumberOfPlayers());
					Server::loadStageOne();
					logger.Log(Logger::Level::Info, "Loaded stage one!");

				}
				if (exitButtonPos.contains(MousePos.x, MousePos.y)) {
					window.close();
				}

			}
		}

		window.clear();
		window.draw(sprite);
		window.draw(playButton);
		window.draw(exitButton);

		window.display();
	}
}
void Game::createClients(uint8_t clients)
{
	for (uint8_t i = 0; i < clients-1; ++i)
	{
		auto client = std::make_shared<PsychicClient>(createWindow());
		m_clients.push_back(client);
		Server::connectAsPsychic(client);
		logger.Log(Logger::Level::Info, "One psychic was connect!");
	}

	auto client = std::make_shared<GhostClient>(createWindow());
	m_clients.push_back(client);
	Server::connectAsGhost(client);
	logger.Log(Logger::Level::Info, "The ghost was connect!");

	Server::initializeClients();
}

void Game::initializeVariables()
{
	m_dt = 0.0f;
	m_exit = false;

	Stage::initializeTextures();
}

void Game::initializeGraphics()
{
	m_gfxSettings.LoadFromFile("../External/Resources/Config/graphics.ini");
}

void Game::run()
{
	while (!m_exit)
	{
		updateDeltaTime();
		update();
		render();
	}
	logger.Log(Logger::Level::Info, "Game Over!");
}

std::shared_ptr<sf::RenderWindow> Game::createWindow()
{
	if (m_gfxSettings.m_fullscreen)
	{
		return std::make_shared<sf::RenderWindow>(
			m_gfxSettings.m_resolution,
			m_gfxSettings.m_title,
			sf::Style::Fullscreen
		);
	}
	else
	{
		return std::make_shared<sf::RenderWindow>(
			m_gfxSettings.m_resolution,
			m_gfxSettings.m_title,
			sf::Style::Titlebar | sf::Style::Close
		);
	}
}

void Game::update()
{
	Server::update();

	for (auto& client : m_clients)
	{
		if (!client->isWindowOpened())
		{
			m_exit = true;
			break;
		}
		client->update(m_dt);
	}
}

void Game::render()
{
	for (auto& client : m_clients)
	{
		client->render();
	}
}

void Game::updateDeltaTime()
{
	m_dt = m_clock.restart().asSeconds();
}