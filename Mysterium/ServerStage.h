#pragma once

#include "PsychicClient.h"
#include "GhostClient.h"

class ServerStage
{
public:
	ServerStage(std::vector<std::shared_ptr<PsychicClient>>& psychicClients, std::shared_ptr<GhostClient> ghostClient);

	virtual void update() = 0;

protected:
	virtual bool waitingForClients() = 0;
	virtual void signalGameStateChange() = 0;

protected:
	bool gameOver;

	std::vector<std::shared_ptr<PsychicClient>>& psychicClients;
	std::shared_ptr<GhostClient> ghostClient;

	uint8_t actionsPerformed;
	// initialize this with 0, increment every time something happens during the current game state. 
	// when it reaches the desired value, the session ends and then reset with 0
};

