#pragma once

#include "Board.h"
#include "Stage.h"
#include "ServerStageTwo.h"
#include "Tile.h"

class PsychicStageTwo : public Stage
{
public:
	PsychicStageTwo(std::shared_ptr<sf::RenderWindow> window);

	void update(const float& dt) override;
	void render(sf::RenderTarget* target = nullptr) override;

	// client-server
	void changeGameState(StageTwoState state);
	void receiveVisionCards(const std::vector<std::string>& cardNames);

private:
	void initializeText();
	void initializeButtons() override;
	void initializeBackground() override;

	void updateButtons() override;

	// client-server
	void sendVote(Color circumstance); // sends the circumstance to the server

private:
	StageTwoState gameState;

	std::unique_ptr<Board> board;
	std::vector<std::unique_ptr<Tile>> visionCards;
	bool voted; // initialize as false, set true after it sends the circumstance vote to the server
};

