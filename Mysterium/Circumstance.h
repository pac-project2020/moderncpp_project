#pragma once
#include "Tile.h"
#include "Button.h"

class Circumstance : public Button
{
private:
	std::shared_ptr<Tile> characterCard;
	std::shared_ptr<Tile> locationCard;
	std::shared_ptr<Tile> objectCard;

	uint8_t faceDown;

public:
	Circumstance(std::shared_ptr<Tile> characterCard, std::shared_ptr<Tile> locationCard, std::shared_ptr<Tile> objectCard);
	~Circumstance();

	std::shared_ptr<Tile> getCharacterCard();
	std::shared_ptr<Tile> getLocationCard();
	std::shared_ptr<Tile> getObjectCard();
	uint8_t getFaceDown();

	void setCharacterCard(std::shared_ptr<Tile>& charCard);
	void setLocationCard(std::shared_ptr<Tile>& locCard);
	void setObjectCard(std::shared_ptr<Tile>& objCard);

	void flipNextCard();
	void initializeAsButton();

	void render(sf::RenderTarget* target);
};

