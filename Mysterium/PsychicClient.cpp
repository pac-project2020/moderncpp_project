#include "Headers.h" // this has to be on the very first line !!!!

#include "PsychicClient.h"
#include "PsychicStageOne.h"
#include "PsychicStageTwo.h"


std::map<Color, std::string> PsychicClient::colorToString =
{
	{Color::Red, "Red"},
	{Color::Blue, "Blue"},
	{Color::Green, "Green"},
	{Color::Purple, "Purple"}
};

PsychicClient::PsychicClient(std::shared_ptr<sf::RenderWindow> window) : Client(std::move(window))
{
	color =Color::Blue;
	clairevoyancePoints=0;
	clairevoyanceTokens=0;
}

Color PsychicClient::getColor()
{
	return color;
}

void PsychicClient::setColor(uint8_t c)
{
	color = (Color)c;
}

void PsychicClient::loadStageOne()
{
	if (m_currentStage)
		m_currentStage.reset();

	m_currentStage = std::make_shared<PsychicStageOne>(m_window, color);
}

void PsychicClient::loadStageTwo()
{
	if (m_currentStage)
		m_currentStage.reset();
	m_currentStage = std::make_shared<PsychicStageTwo>(m_window);
}
