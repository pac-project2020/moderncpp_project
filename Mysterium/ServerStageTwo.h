#pragma once
#include "ServerStage.h"
#include "GhostClient.h"
#include "PsychicClient.h"

enum class StageTwoState : uint8_t
{
	PickingCircumstance,
	RecivingVisionCards,
	Voting,
	Results
};

class ServerStageTwo : public ServerStage
{
public:
	ServerStageTwo(std::vector<std::shared_ptr<PsychicClient>>& psychicClients, std::shared_ptr<GhostClient> ghostclient, std::map<Color, uint8_t> clarvoyancePoints);

	void update() override;

	// client-server
	void receiveCorrectCircumstance(Color circumstance); // receive from the Ghost
	void receiveVote(Color circumstance); // receive guessed from the Psychics
	void giveVisionCards(const std::vector<std::string>& cards); // the Ghost gives the Psychics vision cards

private:
	bool waitingForClients() override;
	uint8_t getVisionCardsCount(uint8_t clarvoyancePoints);
	bool hasWon();

	// client-server
	void signalGameStateChange() override;
	void giveResults();

private:
	StageTwoState gameState;

	std::map<Color, uint8_t> clarvoyancePoints;
	Color correctCircumstance;
	std::vector<Color> psychicGuesses;
};

