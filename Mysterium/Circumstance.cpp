#include "Headers.h" // this has to be on the very first line !!!!

#include "Circumstance.h"
#include "Card.h"

Circumstance::Circumstance(std::shared_ptr<Tile> characterCard, std::shared_ptr<Tile> locationCard, std::shared_ptr<Tile> objectCard)
	: characterCard(characterCard), locationCard(locationCard), objectCard(objectCard)
{
	faceDown = 0;
}

Circumstance::~Circumstance()
{
	//delete characterCard;
	//delete objectCard;
	//delete locationCard;
}

std::shared_ptr<Tile> Circumstance::getObjectCard()
{
	return objectCard;
}

uint8_t Circumstance::getFaceDown()
{
	return faceDown;
}

std::shared_ptr<Tile> Circumstance::getCharacterCard()
{
	return characterCard;
}

std::shared_ptr<Tile> Circumstance::getLocationCard()
{
	return locationCard;
}

void Circumstance::setObjectCard(std::shared_ptr<Tile>& objCard)
{
	objectCard = objectCard;
}

void Circumstance::setCharacterCard(std::shared_ptr<Tile>& charCard)
{
	characterCard = characterCard;
}

void Circumstance::setLocationCard(std::shared_ptr<Tile>& locCard)
{
	locationCard = locationCard;
}

void Circumstance::flipNextCard()
{
	switch (faceDown)
	{
	case 0:
	{
		characterCard->setTexture(Card::getBackTexture());
		break;
	}
	case 1:
	{
		locationCard->setTexture(Card::getBackTexture());
		break;
	}
	case 2:
	{
		objectCard->setTexture(Card::getBackTexture());
		break;
	}
	}

	faceDown++;
}

void Circumstance::initializeAsButton()
{
	/* Approximate positioning of the cards:
	
	 __________________________
	|                          |
	|                          |
	|                          |
	|      CHARACTER CARD      |
	|                          |
	|                          |
	|__________________________|
	 _____________________  ________
	|                     ||        |
	|                     ||        |
	|    LOCATION CARD    || OBJECT |
	|                     ||  CARD  |
	|_____________________||________|
	                                
	The button rectangle goes from the upper left corner of the character card to the lower right corner of the object card
	
	*/

	sf::Vector2f objectCardCoord = objectCard->getPosition(); // upper left
	sf::Vector2f objectCardDim = objectCard->getDimension(); // width and height of the object card

	sf::Vector2f origin = characterCard->getPosition();
	int width = (objectCardCoord.x + objectCardDim.x) - origin.x; // charactercard.lowerRight.x - objectCard.upperLeftCorner.x
	int height = (objectCardCoord.y + objectCardDim.y) - origin.y; // charactercard.lowerRight.y - objectCard.upperLeftCorner.y


	shape.setPosition(origin);
	shape.setSize(sf::Vector2f((float)width,(float)height));
	shape.setOutlineColor(sf::Color::Green);
	shape.setOutlineThickness(3);
}

void Circumstance::render(sf::RenderTarget* target)
{
	characterCard->render(target);
	locationCard->render(target);
	objectCard->render(target);

	//Button::render(target);
}
