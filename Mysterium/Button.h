#pragma once

#include<iostream>

class Button
{
protected:
	enum State
	{
		Idle=0,
		Active,
	};

	bool state : 1;
	sf::RectangleShape shape;

public:
	
	Button();
	Button(float width, float height);
	Button(float x, float y, float width, float height);
	Button(float x, float y, sf::Texture* buttonTexture);
	const bool isPressed() const;
	void update(const sf::Vector2i& mousePos);
    void render(sf::RenderTarget* target);

	void setPosition(float x, float y);
	void setScale(float x, float y);

	sf::Vector2f getPosition();
	sf::Vector2f getDimension();
	sf::Texture getTexture();
	bool getState();

	void reset();
};

