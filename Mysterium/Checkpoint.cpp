#include "Headers.h" // this has to be on the very first line !!!!

#include "Checkpoint.h"

Checkpoint::Checkpoint(int x, int y, const sf::Texture& texture, std::vector<std::shared_ptr<Card>>& cards) : 
	Tile(x, y, texture), 
	cards(cards)
{
	setCardsPositions(x,y);
}

Checkpoint::Checkpoint(int x, int y, const sf::Texture& texture) : Tile(x, y, texture)
{
}

void Checkpoint::insertToken(std::shared_ptr<Token> token)
{
	sf::Vector2i offset (10,10); // offset from the top left corner of the checkpoint sprite
	int padding = 40; // distance between tokens

	auto origin = m_sprite.getPosition(); // the top left corner of the checkpoint sprite

	int x = origin.x + offset.x + padding * tokens.size();
	int y = origin.y + offset.y;

	token->setPosition(x, y);
	tokens.push_back(token);
}

void Checkpoint::moveToken(Color color, std::string cardName)
{
	std::shared_ptr<Card> card = nullptr;

	for (auto& c : cards)
	{
		if (c->getName() == cardName)
		{
			card = c;
			break;
		}

	}

	if (!card)
		throw "ERROR::CHECKPOINT::COULD_NOT_FIND_CARD";

	for (auto it = tokens.begin(); it != tokens.end(); ++it)
	{
		auto token = *it;
		if (token->getColor() == color)
		{
			tokens.erase(it);
			card->insertToken(token);

			return;
		}
	}
}

void Checkpoint::resetCardTokens()
{
	for (auto& card : cards)// remove the tokens from each card and place them back on the checkpoint sprite
	{
		auto cardTokens = card->getTokensVector(); 

		if (cardTokens.size() > 0)
		{
			for (auto token : cardTokens)
				insertToken(token);

			card->clear();
		}
	}
}

std::shared_ptr<Token> Checkpoint::removeToken(Color color)
{
	for (auto& card : cards) // look through the vectors contained in the cards
	{
		if (card->containsToken(color))
		{
			auto token = card->removeToken(color);
			return token;
		}
	}

	return nullptr;
}

bool Checkpoint::containsOnCard(Color color)
{
	for (auto& card : cards) // look through the vectors contained in the cards
	{
		if (card->containsToken(color))
		{
			return true;
		}
	}

	return false;
}

bool Checkpoint::contains(Color color)
{
	for (auto& token : tokens)
	{
		if (token->getColor() == color)
			return true;
	}

	return false;
}

std::shared_ptr<Card> Checkpoint::getPlayerChoice()
{
	for (auto& card : cards)
	{
		if (card->isPressed())
		{
			std::shared_ptr<Card> c = card;
			card->reset();

			return c;
		}
	}

	return nullptr;
}

std::vector<std::shared_ptr<Card>> Checkpoint::getCards()
{
	return cards;
}

std::vector<std::shared_ptr<Token>> Checkpoint::getTokens()
{
	return tokens;
}

void Checkpoint::update(const sf::Vector2i mousePosition)
{
	for (auto& card : cards)
	{
		card->update(mousePosition);
	}
}

void Checkpoint::render(sf::RenderTarget* target)
{
	Tile::render(target);

	for (auto& token : tokens)
	{
		token->render(target);
	}

	for (auto& card : cards)
	{
		card->render(target);
	}
}

void Checkpoint::setCardsPositions(int originX, int originY)
{
	int y = originY - 200;
	int padding = 280;

	for (uint8_t i = 0; i < cards.size(); ++i)
	{
		int x = originX + i * padding;
		cards[i]->setPosition(x, y);
	}
}
