#include "Headers.h" // this has to be on the very first line !!!!

#include "Stage.h"
#include "Constants.h"

std::map<std::string, sf::Texture> Stage::m_textures;

std::vector<std::string> Stage::characterCardNames;
std::vector<std::string> Stage::locationCardNames;
std::vector<std::string> Stage::objectCardNames;

std::vector<std::string> Stage::cardNames;
std::vector<std::string> Stage::visionCardNames;


Stage::Stage(std::shared_ptr<sf::RenderWindow> window) : m_window(std::move(window))
{
	keyTime = 0.f;
	keyTimeMax = 10.f;
	gameOver = false;
}

Stage::~Stage()
{
}

bool Stage::readCardNames(std::string fileName)
{
	std::ifstream in(fileName.c_str());
	
	if (!in)
	{
		std::cerr << "Cannot open the File : " << fileName << std::endl;
		return false;
	}
	std::string str;
	
	while (std::getline(in, str))
	{
		if (str.size() > 0)
			cardNames.push_back(str);
	}
	
	in.close();
	return true;

}

void Stage::readVisionCards()
{
	std::stringstream ss;

	for (int i = 1; i <= noVisionCards; ++i) 
	{
		ss << "visionCard (" << i << ").jpg";
		visionCardNames.push_back(ss.str());
		ss.str(" ");
	}
}

std::vector<std::string> Stage::getVisionCardNames()
{
	return visionCardNames;
}

void Stage::setGameOver(bool won)
{
	gameOver = true;
	if(won)
		infoText.setString("Game Over. You won");
	else
		infoText.setString("Game Over. You lost");
}

void Stage::initializeTextures()
{
	readCardNames("cardNames.txt");

	readVisionCards();

	uint8_t counterType = 1;

	for (auto& name : Stage::cardNames)
	{
		if (counterType >= characterRange.first && counterType <= characterRange.second)
		{
			if (!m_textures[name].loadFromFile("../External/Resources/Cards/CharacterCards/" + name))
			{
				throw "ERROR::STAGE::COULD_NOT_LOAD_" + name + "_TEXTURE";
			}

			characterCardNames.push_back(name);
		}
		else if (counterType >= locationRange.first && counterType <= locationRange.second)
		{
			if (!m_textures[name].loadFromFile("../External/Resources/Cards/LocationCards/" + name))
			{
				throw "ERROR::STAGE::COULD_NOT_LOAD_" + name + "_TEXTURE";
			}

			locationCardNames.push_back(name);
		}
		else if (counterType >= objectRange.first && counterType <= objectRange.second)
		{
			if (!m_textures[name].loadFromFile("../External/Resources/Cards/ObjectCards/" + name))
			{
				throw "ERROR::STAGE::COULD_NOT_LOAD_" + name + "_TEXTURE";
			}

			objectCardNames.push_back(name);
		}

		if (counterType < objectRange.second) //todo constant
		{
			++counterType;
		}
		else
			break;

	}


	for (std::string name : visionCardNames)
	{
		if (!m_textures[name].loadFromFile("../External/Resources/Cards/VisionCards/" + name))
		{
			throw "ERROR::STAGE::COULD_NOT_LOAD_" + name + "_TEXTURE";
		}
	}


	if (!m_textures["CARD_BACK"].loadFromFile("../External/Resources/Cards/cardBack.jpg"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_CARD_BACK_TEXTURE";
	}

	// tokens

	if (!m_textures["TOKEN_RED"].loadFromFile("../External/Resources/Psychics/token_red.jpg"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_PSYCHIC1_TEXTURE";
	}

	if (!m_textures["TOKEN_BLUE"].loadFromFile("../External/Resources/Psychics/token_blue.jpg"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_PSYCHIC2_TEXTURE";
	}

	if (!m_textures["TOKEN_GREEN"].loadFromFile("../External/Resources/Psychics/token_green.jpg"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_PSYCHIC3_TEXTURE";
	}

	if (!m_textures["TOKEN_PURPLE"].loadFromFile("../External/Resources/Psychics/token_purple.jpg"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_PSYCHIC4_TEXTURE";
	}


	//game bkgr

	if (!m_textures["BACKGROUND"].loadFromFile("../External/Resources/Background/woodtexture.jpg"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_GAME_BACKGROUND";
	}

	//checkpoint

	if (!m_textures["CHECKPOINT"].loadFromFile("../External/Resources/Background/checkpoint.png"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_CHECKPOINT_TEXTURE";
	}

	// buttons

	if (!m_textures["BUTTON_CORRECT"].loadFromFile("../External/Resources/Buttons/button_correct.jpg"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_BUTTON_TEXTURE";
	}

	if (!m_textures["BUTTON_INCORRECT"].loadFromFile("../External/Resources/Buttons/button_incorrect.jpg"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_BUTTON_TEXTURE";
	}

	if (!m_textures["BUTTON_SKIP"].loadFromFile("../External/Resources/Buttons/button_skip.jpg"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_BUTTON_TEXTURE";
	}

	if (!m_textures["CROW_BUTTON"].loadFromFile("../External/Resources/Buttons/button_crow.jpg"))
	{
		throw "ERROR::PSYCHIC_STAGE_ONE::COULD_NOT_LOAD_BUTTON_TEXTURE";
	}
}

void Stage::SetText(sf::Text& text, const sf::Font& font, const sf::Color& fillColor, uint16_t size, const sf::Vector2f& position, float thickness)
{
	text.setFillColor(fillColor);
	text.setFont(font);
	text.setCharacterSize(size);
	text.setPosition(position);
	text.setOutlineThickness(thickness);
	text.setOutlineColor(fillColor);
}

void Stage::SetText(sf::Text& text, const sf::Font& font, const sf::Color& fillColor, uint16_t size, float thickness)
{
	text.setFillColor(fillColor);
	text.setFont(font);
	text.setCharacterSize(size);
	text.setOutlineThickness(thickness);
	text.setOutlineColor(fillColor);
}

void Stage::updateMousePosition()
{
	mousePosScreen = sf::Mouse::getPosition();
	mousePosWindow = sf::Mouse::getPosition(*m_window);
}
void Stage::updateKeyTime(const float& dt)
{
	if (keyTime < keyTimeMax)
		keyTime += 100.f * dt;
}
bool Stage::getKeyTime()
{
	if (keyTime >= keyTimeMax)
	{
		keyTime = 0.f;
		return true;
	}

	return false;
}
void Stage::renderButtons(sf::RenderTarget& target)
{
	for (auto& it : m_buttons)
	{
		it.second->render(m_window.get());
	}
}

