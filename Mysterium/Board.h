#pragma once
#include "PsychicClient.h"
#include "Circumstance.h"
#include "Card.h"

class Board
{
private:
	std::map<Color, std::unique_ptr<Circumstance>> psychicCircumstances; //map de circumstante ptr fiecare psychic
	std::map<std::string, sf::Texture>& m_textures; //mapa de texturi

public:
	Board(std::map<std::string, sf::Texture>& textures, const std::map<Color, std::tuple<std::string, std::string, std::string>>& circumstances);

	void update(std::vector<Color> correctlyPlacedTokens);
	void update(sf::Vector2i mousePos);
	void render(sf::RenderTarget* target);

	// for picking a circumstance in stage 2
	bool pickedCircumstance(); // checks to see if the ghost picked a circumstance
	Color getChoice(); // returns the color of the picked circumstance
	void clearUnusedCircumstances(); // removes the circumstanced that aren't used in stage 2 (after picking a circumstance)
	void initializeCircumstancesAsButtons(); 

private:
	void setCircumstancePositions();
};
