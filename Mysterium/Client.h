#pragma once


#include "Stage.h"

class Client
{
public:
	Client(std::shared_ptr<sf::RenderWindow> window);
	~Client();

	void update(const float& dt);
	void render();
	bool isWindowOpened();

	std::shared_ptr<Stage> getCurrentStage();

	virtual void loadStageOne() = 0;
	virtual void loadStageTwo() = 0;

private:
	void updateSfmlEvents();

protected:
	std::shared_ptr<Stage> m_currentStage;
	std::shared_ptr<sf::RenderWindow> m_window;

private:
	sf::Event m_event;
};

