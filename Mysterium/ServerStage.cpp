#include "Headers.h" // this has to be on the very first line !!!!

#include "ServerStage.h"

ServerStage::ServerStage(std::vector<std::shared_ptr<PsychicClient>>& pClients, std::shared_ptr<GhostClient> gClient)
	: psychicClients(pClients), ghostClient(std::move(gClient))
{
	gameOver = false;
	actionsPerformed = 0;
}
