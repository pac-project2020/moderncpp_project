#pragma once
#include <vector>
#include "CircumstanceCard.h"
#include "VisionCard.h"

class Deck
{
public:
	Deck(std::string filename);

	//void Shuffle();

private:
	std::vector<Card> m_cards;
};

