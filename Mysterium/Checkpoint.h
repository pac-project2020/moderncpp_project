#pragma once
#include "Token.h"
#include "Card.h"
#include "PsychicClient.h"

class Checkpoint : public Tile
{
private:
	std::vector<std::shared_ptr<Token>> tokens; // the tokens placed on the checkpoint
	std::vector<std::shared_ptr<Card>> cards;   // each card has a vector of tokens

public:
	Checkpoint(int x, int y, const sf::Texture& texture, std::vector<std::shared_ptr<Card>>& cards);
	Checkpoint(int x, int y, const sf::Texture& texture);

	void insertToken(std::shared_ptr<Token> token); // adds a new token to this checkpoint
	void moveToken(Color token, std::string card); // moves a token (that is already on this checkpoint) to a card
	void resetCardTokens(); // moved the tokens back to the checkpoint
	std::shared_ptr<Token> removeToken(Color color); // removes a token from a checkpoint (in order to move it to the next one inside the PsychicStageOne)
	bool containsOnCard(Color color);
	bool contains(Color color);

	std::shared_ptr<Card> getPlayerChoice();
	std::vector<std::shared_ptr<Card>> getCards();
	std::vector<std::shared_ptr<Token>> getTokens();

	void update(const sf::Vector2i mousePosition);
	void render(sf::RenderTarget* target);

private:
	void setCardsPositions(int originX, int originY);
};

