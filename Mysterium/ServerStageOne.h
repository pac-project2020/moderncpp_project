#pragma once
#include <iostream>
#include "Button.h"
#include "GhostClient.h"
#include "PsychicClient.h"
#include "ServerStage.h"
#include "../Logging/Logging.h"

enum class StageOneState
{
	ReceivingVisionCards,
	Voting,
	ClairevoyanceVoting,
	Results
};

class ServerStageOne : public ServerStage
{
	using Circumstance = std::tuple<std::string, std::string, std::string>;
	using Vote = std::tuple<Color, bool>;

private:
	StageOneState gameState;
	uint8_t maxTurns = 7;
	uint8_t turn;

	std::map<Color, Circumstance>& circumstances;
	std::map<Color, uint8_t> clarvoyancePoints; // holds the clarvoyance points for each psychic

	std::map<Color, std::string> tokenPositions; // holds the card choices made by psychics each turn
	std::vector<Color> correctlyPlacedTokens; // holds the tokens that can advance

	std::map<Color, uint8_t> checkpoints;
	uint8_t correctGuesses; // how many psychics guessed all 3 cards

public:
	ServerStageOne(std::vector<std::shared_ptr<PsychicClient>>& psychicClients, std::shared_ptr<GhostClient> ghostClient, std::map<Color, Circumstance>& circumstances);

	bool checkForEndStage();
	void updateTurn();
	void handleStageEnd(); //last turn, everyone guessed or not
	void update() override; 

	void startStageOne();

	// client-server stuff
	void giveVisionCard(Color psychic, std::string card); // the ghost will call this when giving vision cards to each psychic
	void voteForClarvoyance(Color psychic, std::vector<Vote> votes);
	void skipClarvoyanceVote();
	void placeToken(Color psychic, std::string card);

private:
	bool waitingForClients() override;
	void signalGameStateChange() override;
	void sendTokenResults();
	void sendClarvoyancePoints();
	void calculateTokenResults();
};

