#pragma once
#include "Stage.h"
#include "PsychicStageOne.h"
#include "Card.h"
#include "Constants.h"
#include "Checkpoint.h"
#include "ServerStageOne.h"

class PsychicStageOne : public Stage
{
	using Vote = std::tuple<Color, bool>;

public:
	PsychicStageOne(std::shared_ptr<sf::RenderWindow> window, Color color);
	~PsychicStageOne();

	void update(const float& dt) override;
	void render(sf::RenderTarget* target = nullptr)override;

	void initializeCheckpoints();
	void changeGameState(StageOneState state);
	void recieveTokenResults(const std::vector<Color>& results);
	void recieveVisionCard(Color color, std::string card);
	void recieveTokenPlacement(Color color, std::string card);
	void setClarvoyanceTokens(uint8_t number);
	void updateTurn(uint8_t turn);

private:
	// initialize things
	void initializeText();
	void initializeBackground() override;
	void initializeButtons() override;

	// update things
	void updateButtons() override;
	void updateInfoText();
	void updateClarvoyanceText();

	void sendClarvoyanceVotes();
	void updateCurrentPsychicColor();

private:
	std::vector<std::unique_ptr<Checkpoint>> checkpoints;
	std::map<Color,std::vector<std::shared_ptr<Tile>>> visionCards;

	StageOneState gameState;

	uint8_t currentCheckpoint;
	Color psychicColor;
	bool pickedCard;

	sf::Text currentTurn;
	sf::Text clarvoyanceTokensText;

	std::map<std::string, std::unique_ptr<Button>> clarvoyanceButtons;
	std::vector<Vote> clarvoyanceVotes;
	uint8_t currentPsychicColor; // used in clarvoyance voting
	uint8_t clarvoyanceTokens;
	bool votedForClarvoyance;

	bool guessedAllCards;
};

