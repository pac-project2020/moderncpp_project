#include "Headers.h" // this has to be on the very first line !!!!

#include "GhostStageTwo.h"
#include "Server.h"

GhostStageTwo::GhostStageTwo(std::shared_ptr<sf::RenderWindow> window) : Stage(std::move(window))
{
	sentCircumstance = false;

	initializeBackground();
	initializeButtons();
	initializeText();
}

void GhostStageTwo::update(const float& dt)
{
	if (gameOver)
		return;

	updateMousePosition();
	updateButtons();

	switch (gameState)
	{
	case StageTwoState::PickingCircumstance:
	{
		if (!sentCircumstance)
			if (board->pickedCircumstance())
			{
				auto choice = board->getChoice();
				sendChoice(choice);
				board->clearUnusedCircumstances();
			}

		break;
	}
	case StageTwoState::RecivingVisionCards:
	{
		if (pickedEnoughCards())
		{
			// send them to the server
			sendVisionCards();
		}
		else
		{
			// for each position in the array of cards
			//		if the position is not null and the card on that position was pressed
			//			add the name of the card in the visionCardsToSend vector	
			//			delete the card	and set the array position to null

			for (uint8_t i = 0; i < ghostHand; ++i)
			{
				if (visionCardsInHand.at(i) && visionCardsInHand.at(i)->isPressed())
				{
					visionCardsToSend.push_back(visionCardsInHand.at(i)->getName());
					visionCardsInHand.at(i).reset();
				}
			}
		}
		break;
	}
	}
}

void GhostStageTwo::render(sf::RenderTarget* target)
{
	if (!target)
		target = m_window.get();

	target->draw(m_background);

	// render cards
	for (uint8_t i = 0; i < ghostHand; ++i)
	{
		if (visionCardsInHand.at(i))
			visionCardsInHand.at(i)->render(target);
	}

	// render circumstance(s)
	board->render(target);

	// render the info text
	target->draw(infoText);
}

void GhostStageTwo::changeGameState(StageTwoState state)
{
	gameState = state;

	switch (gameState)
	{
	case StageTwoState::PickingCircumstance:
	{
		infoText.setString("Pick a Circumstance");
		break;
	}
	case StageTwoState::RecivingVisionCards:
	{
		infoText.setString("Pick three Vision cards");
		break;
	}
	case StageTwoState::Voting:
	{
		infoText.setString("Waiting for psychics...");
		break;
	}
	case StageTwoState::Results:
	{
		infoText.setString("");
		break;
	}
	}
}

void GhostStageTwo::initializeText()
{
	if (!font.loadFromFile("../External/Resources/Fonts/arial.ttf"))
	{
		throw "ERROR::STAGE::COULD_NOT_LOAD_FONT";
	}

	SetText(infoText, font, sf::Color::White, 30, sf::Vector2f(10.0f, 10.0f));
}

void GhostStageTwo::initializeBoard()
{
	auto stringCircumstances = Server::getCircumstances();
	board = std::make_unique<Board>(m_textures, stringCircumstances);
	board->initializeCircumstancesAsButtons();
}

void GhostStageTwo::initializeCards()
{
	sf::Vector2i offset(200, 200);
	int padding = 220;

	int counter = 0;
	for (auto& card : visionCardsInHand)
	{
		if (!visionCardsInHand.at(counter))
		{
			int x = offset.x + counter * padding;
			int y = offset.y;

			std::string cardName = Server::requestVisionCard();
			auto card = std::make_unique<Card>(x, y, m_textures[cardName], cardName);
			card->setPosition((float)x,(float)y);
			card->setScale(0.5, 0.5);

			visionCardsInHand.at(counter) = std::move(card);
		}
		counter++;
	}
}

void GhostStageTwo::initializeButtons()
{
	initializeBoard();
	initializeCards();
}

void GhostStageTwo::initializeBackground()
{
	m_background.setSize(sf::Vector2f(m_window->getSize()));
	m_background.setTexture(&m_textures["BACKGROUND"]);
}

void GhostStageTwo::updateButtons()
{
	switch (gameState)
	{
	case StageTwoState::PickingCircumstance:
	{
		if (!pickedEnoughCards())
			board->update(mousePosWindow);
		break;
	}
	case StageTwoState::RecivingVisionCards:
	{
		for (uint8_t i = 0; i < ghostHand; ++i)
		{
			if (visionCardsInHand.at(i))
			{
				visionCardsInHand.at(i)->update(mousePosWindow);
			}
		}

		break;
	}
	}
}

bool GhostStageTwo::pickedEnoughCards()
{
	return visionCardsToSend.size() > 2;
}

void GhostStageTwo::sendChoice(Color circumstance)
{
	auto stage = std::dynamic_pointer_cast<ServerStageTwo>(Server::getCurrentStage());
	if (stage)
	{
		stage->receiveCorrectCircumstance(circumstance);
	}
}

void GhostStageTwo::sendVisionCards()
{
	auto stage = std::dynamic_pointer_cast<ServerStageTwo>(Server::getCurrentStage());
	if (stage)
	{
		stage->giveVisionCards(visionCardsToSend);
	}
}
