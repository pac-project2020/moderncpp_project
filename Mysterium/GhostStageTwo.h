#pragma once

#include "Constants.h"
#include "Stage.h"
#include "Circumstance.h"
#include "PsychicClient.h"
#include "Board.h"
#include "ServerStageTwo.h"


class GhostStageTwo : public Stage
{
public:
	GhostStageTwo(std::shared_ptr<sf::RenderWindow> window);

	void update(const float& dt) override;
	void render(sf::RenderTarget* target = nullptr) override;
	
	// client-server
	void changeGameState(StageTwoState state);

private:
	void initializeText();
	void initializeBoard();
	void initializeCards();
	void initializeButtons() override;
	void initializeBackground() override;

	void updateButtons() override;

	bool pickedEnoughCards();

	// client-server
	void sendChoice(Color circumstance);
	void sendVisionCards();

private:
	std::unique_ptr<Board> board;
	std::array<std::unique_ptr<Card>, ghostHand> visionCardsInHand;
	std::vector<std::string> visionCardsToSend; // the name of the cards that will be given to the psychics

	bool sentCircumstance;
	StageTwoState gameState;
};

