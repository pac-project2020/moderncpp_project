#include "Headers.h" // this has to be on the very first line !!!!

#include "GhostStageOne.h"
#include "Server.h"

GhostStageOne::GhostStageOne(std::shared_ptr<sf::RenderWindow> window, std::vector<Color>& pColors) :
	Stage(std::move(window)), 
	psychics(pColors)
{
	crowMarkers = 3;

	initializeButtons();
	initializeBackground();
	initializeText();

	Card::setBackTexture(m_textures["CARD_BACK"]);

	initializeBoard();
	drawVisionCards();
}

GhostStageOne::~GhostStageOne()
{
	for (uint8_t i = 0; i < visionCardsInHand.size(); ++i)
	{
		visionCardsInHand.at(i).reset();
	}
}

void GhostStageOne::updateButtons()
{
	if (crowMarkers > 0)
		crowMarkerButton->update(mousePosWindow);
}

void GhostStageOne::onCrowButtonPressed()
{
	for (uint8_t i = 0; i < visionCardsInHand.size(); ++i)
	{
		visionCardsInHand.at(i).reset();
	}

	drawVisionCards();

	--crowMarkers;

	std::stringstream ss;
	ss << "Crow markers: " << std::to_string(crowMarkers);
	crowMarkersText.setString(ss.str());
}

void GhostStageOne::update(const float& dt)
{
	if (gameOver)
		return;

	updateKeyTime(dt);

	if (gameState == StageOneState::ReceivingVisionCards && getKeyTime())
	{
		updateButtons();
		updateMousePosition();

		// update card buttons
		for(auto& card : visionCardsInHand)
		{
			card->update(mousePosWindow);
		}

		for (auto& card : visionCardsInHand)
		{
			if (card->isPressed()) // if the ghost picked this card
			{
				auto stage = std::dynamic_pointer_cast<ServerStageOne>(Server::getCurrentStage());

				if (stage)
				{
					stage->giveVisionCard(psychics.at(currentPsychicIndex++), card->getName());
				}

				card.reset();

				drawVisionCards(); // refill hand

				if (currentPsychicIndex < psychics.size())
				{
					std::stringstream ss;
					ss << "Pick a vision card for psychic #" << currentPsychicIndex + 1;
					infoText.setString(ss.str());
					logger.Log(Logger::Level::Info, "The ghost was pick a vision card for psychic " + (currentPsychicIndex + 1));
				}

				break;
			}
		}

		if (crowMarkers > 0 && crowMarkerButton->isPressed())
		{
			onCrowButtonPressed();
			crowMarkerButton->reset();
		}
	}
}

void GhostStageOne::render(sf::RenderTarget* target)
{
	if (!target)
		target = m_window.get();

	target->draw(m_background);

	// render the circumstances
	board->render(target);

	// render the vision cards
	for (auto& card : visionCardsInHand)
	{
		card->render(target);
	}

	// render the crow marker button thingy if the ghost has crow markers left
	if (crowMarkers > 0)
		crowMarkerButton->render(target);

	// render the crow markers text
	target->draw(crowMarkersText);

	// render the info text
	target->draw(infoText);

	// render the current turn
	target->draw(currentTurn);
}

void GhostStageOne::changeGameState(StageOneState state)
{
	gameState = state;

	switch (gameState)
	{
	case StageOneState::ReceivingVisionCards:
	{
		currentPsychicIndex = 0;

		infoText.setString("Pick a vision card for psychic #1");

		break;
	}
	case StageOneState::Voting:
	{
		infoText.setString("Waiting for psychic votes...");

		break;
	}
	case StageOneState::ClairevoyanceVoting:
	{
		break;
	}
	case StageOneState::Results:
	{
		infoText.setString("");

		break;
	}
	}
}

void GhostStageOne::recieveTokenResults(std::vector<Color> correctlyPlacedTokens)
{
	if (correctlyPlacedTokens.size() > 0)
		board->update(correctlyPlacedTokens);
}

void GhostStageOne::updateTurn(uint8_t turn)
{
	std::stringstream ss;
	ss << "Current turn: " << std::to_string(turn);

	currentTurn.setString(ss.str());
}

void GhostStageOne::initializeText()
{
	if (!font.loadFromFile("../External/Resources/Fonts/arial.ttf"))
	{
		throw "ERROR::STAGE::COULD_NOT_LOAD_FONT";
	}

	SetText(infoText, font, sf::Color::White, 30, sf::Vector2f(10.0f, 10.0f));
	SetText(currentTurn, font, sf::Color::White, 30, sf::Vector2f(1610.0f, 10.0f));
	SetText(crowMarkersText, font, sf::Color::White, 30, sf::Vector2f(1610.0f, 40.0f));

	currentTurn.setString("Current turn: 1");
	crowMarkersText.setString("Crow markers: 3");
}

void GhostStageOne::initializeBackground()
{
	m_background.setSize(sf::Vector2f(m_window->getSize()));
	m_background.setTexture(&m_textures["BACKGROUND"]);
}

void GhostStageOne::initializeButtons()
{
	crowMarkerButton = std::make_unique<Button>(1600, 80, &m_textures["CROW_BUTTON"]);
}

void GhostStageOne::initializeGhostHand()
{
	int index = 0;
	for (auto& cardName : visionCardNames)
	{
		auto buttonCard{ std::make_unique<Card>(cardName) };

		visionCardsInHand.at(index) = std::move(buttonCard);

		++index;
	}
}

void GhostStageOne::initializeBoard()
{
	auto stringCircumstances = Server::getCircumstances();

	board = std::make_unique<Board>(m_textures, stringCircumstances);
}

void GhostStageOne::drawVisionCards() //draws a card each time the ghost gives one to a Psychic
{
	sf::Vector2i offset(200, 200);
	int padding = 220;

	int counter = 0;
	for (auto& card : visionCardsInHand)
	{
		if (!card)
		{
			int x = offset.x + counter * padding;
			int y = offset.y;

			std::string cardName = Server::requestVisionCard();
			auto card = std::make_unique<Card>(x, y, m_textures[cardName], cardName);
			card->setPosition(x, y);
			card->setScale(0.5, 0.5);

			visionCardsInHand.at(counter) = std::move(card);
		}
		counter++;
	}
}