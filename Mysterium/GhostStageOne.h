#pragma once
#include "Stage.h"
#include "GhostStageOne.h"
#include "Card.h"
#include "Server.h"
#include "Constants.h"
#include "Circumstance.h"
#include "Board.h"

class GhostStageOne : public Stage
{
public:
	GhostStageOne(std::shared_ptr<sf::RenderWindow> window, std::vector<Color>& psychics);
	~GhostStageOne();

	void update(const float& dt) override;
	void render(sf::RenderTarget* target = nullptr) override;

	// client-server stuff
	void changeGameState(StageOneState state);	
	void recieveTokenResults(std::vector<Color> correctlyPlacedTokens);
	void updateTurn(uint8_t turn);

private:
	// initialize things
	void initializeText();
	void initializeBackground() override;
	void initializeButtons() override;
	void initializeGhostHand(); //fac vectorul
	void initializeBoard();
	void drawVisionCards(); //basically refill

	// update things
	void updateButtons() override;
	void onCrowButtonPressed();

private:
	std::array<std::unique_ptr<Card>, ghostHand> visionCardsInHand;
	std::unique_ptr<Board> board;
	uint8_t crowMarkers;

	StageOneState gameState;
	std::vector<Color> psychics;
	uint8_t currentPsychicIndex;

	sf::Text currentTurn;
	sf::Text crowMarkersText;

	std::unique_ptr<Button> crowMarkerButton;
};

