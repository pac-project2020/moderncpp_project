#pragma once

#include "Client.h"

	enum class Color : uint8_t
	{
		Red,
		Blue,
		Green,
		Purple,
	};

class PsychicClient : public Client
{
public:

public:
	PsychicClient(std::shared_ptr<sf::RenderWindow> window);// color or something
	Color getColor();	
	void setColor(uint8_t c);

	void loadStageOne() override;
	void loadStageTwo() override;

public:
	static std::map<Color, std::string> colorToString;

private:
	Color color;

	uint8_t clairevoyancePoints;
	uint8_t clairevoyanceTokens;
	//Circumstance circumstance;
};

