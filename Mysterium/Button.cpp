#include "Headers.h" // this has to be on the very first line !!!!

#include "Button.h"

Button::Button()
{
	state = Idle;
}

Button::Button(float width, float height)
{
	shape.setSize(sf::Vector2f(width, height));

	state = Idle;
}

Button::Button(float x, float y, float width, float height)
{
	shape.setPosition(sf::Vector2f(x, y));
	shape.setSize(sf::Vector2f(width, height));
	shape.setOutlineColor(sf::Color::Green);
	shape.setOutlineThickness(3);

	state = Idle;
}

Button::Button(float x, float y, sf::Texture* texture)
{
	sf::Vector2f size(texture->getSize().x, texture->getSize().y);

	shape.setPosition(sf::Vector2f(x, y));
	shape.setSize(size);
	shape.setTexture(texture, false);
	state = Idle;
}

const bool Button::isPressed() const
{
	if (state == Active)
		return true;

	return false;
}

void Button::update(const sf::Vector2i& mousePos)
{
	state =Idle;

	if (shape.getGlobalBounds().contains(static_cast<sf::Vector2f>(mousePos)))
	{
	
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			
			state = Active;
		}
	}
}

void Button::render(sf::RenderTarget* target)
{
	target->draw(shape);
}

void Button::setPosition(float x, float y)
{
	shape.setPosition(x, y);
}

void Button::setScale(float x, float y)
{
	shape.setScale(x, y);
}

sf::Vector2f Button::getPosition()
{
	return shape.getPosition();
}

sf::Vector2f Button::getDimension()
{
	return shape.getScale();
}

sf::Texture Button::getTexture()
{
	return *shape.getTexture();
}

bool Button::getState()
{
	return state;
}

void Button::reset()
{
	state = Idle;
}
