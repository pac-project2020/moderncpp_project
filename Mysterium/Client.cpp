#include "Headers.h" // this has to be on the very first line !!!!

#include "Client.h"

Client::Client(std::shared_ptr<sf::RenderWindow> window) : m_window(std::move(window))
{
}

Client::~Client()
{
}

void Client::update(const float& dt)
{
	updateSfmlEvents();
	if (m_currentStage)
	{
		m_currentStage->update(dt);
	}
}

void Client::render()
{
	m_window->clear();

	if (m_currentStage)
	{
		m_currentStage->render();
	}

	m_window->display();
	
}

bool Client::isWindowOpened()
{
	return m_window->isOpen();
}

std::shared_ptr<Stage> Client::getCurrentStage()
{
	return m_currentStage;
}


void Client::updateSfmlEvents()
{
	while (m_window->pollEvent(m_event))
	{
		if (m_event.type == sf::Event::Closed)
			m_window->close();
	}
}
