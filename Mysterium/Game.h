#pragma once
#include "../Logging/Logging.h"

#include "Client.h"
#include "Stage.h"
#include "GraphicsSettings.h"

class Game
{
public:
	Game();
	~Game();

	void run();

private:
	void initializeServer();
	void initializeMainMenu();
	void createClients(uint8_t clients);
	std::shared_ptr<sf::RenderWindow> createWindow();

	void initializeVariables();
	void initializeGraphics();

	void update();
	void render();
	void updateDeltaTime();

private:
	GraphicsSettings m_gfxSettings;

	std::vector<std::shared_ptr<Client>> m_clients;

	sf::Clock m_clock;
	float m_dt;

	bool m_exit;

	Logger logger;
};

