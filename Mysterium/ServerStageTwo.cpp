#include "Headers.h" // this has to be on the very first line !!!!

#include "Server.h"
#include "ServerStageTwo.h"
#include "PsychicStageTwo.h"
#include "GhostStageTwo.h"

ServerStageTwo::ServerStageTwo(std::vector<std::shared_ptr<PsychicClient>>& pClients, std::shared_ptr<GhostClient> gClient, std::map<Color, uint8_t> cPoints)
	: ServerStage(pClients, std::move(gClient)), clarvoyancePoints(cPoints)
{
	gameState = StageTwoState::PickingCircumstance;
	signalGameStateChange();
}

void ServerStageTwo::update()
{
	if (gameOver)
		return;

	switch (gameState)
	{
	case StageTwoState::PickingCircumstance:
	{
		if (!waitingForClients())
		{
			actionsPerformed = 0;
			gameState = StageTwoState::RecivingVisionCards;

			signalGameStateChange();
		}

		break;
	}
	case StageTwoState::RecivingVisionCards:
	{
		if (!waitingForClients())
		{
			actionsPerformed = 0;
			gameState = StageTwoState::Voting;

			signalGameStateChange();
		}

		break;
	}
	case StageTwoState::Voting:
	{
		if (!waitingForClients())
		{
			actionsPerformed = 0;
			gameState = StageTwoState::Results;

			signalGameStateChange();
		}

		break;
	}
	case StageTwoState::Results:
	{
		giveResults();

		break;
	}
	}
}

void ServerStageTwo::receiveCorrectCircumstance(Color circumstance)
{
	correctCircumstance = circumstance;
	++actionsPerformed;
}

void ServerStageTwo::receiveVote(Color circumstance)
{
	++actionsPerformed;
	psychicGuesses.push_back(circumstance);
}

void ServerStageTwo::giveVisionCards(const std::vector<std::string>& cards)
{
	++actionsPerformed;

	for (auto& psychic : psychicClients)
	{
		auto stage = std::dynamic_pointer_cast<PsychicStageTwo>(psychic->getCurrentStage());
		if (stage)
		{
			uint8_t cardCount = getVisionCardsCount(clarvoyancePoints[psychic->getColor()]);

			std::vector<std::string> c;
			for (uint8_t i = 0; i < cardCount; ++i)
			{
				c.push_back(cards[i]);
			}

			stage->receiveVisionCards(c);
		}
	}
}

bool ServerStageTwo::waitingForClients()
{
	switch (gameState)
	{
	case StageTwoState::PickingCircumstance:
	{
		return actionsPerformed < 1;
	}
	case StageTwoState::RecivingVisionCards:
	{
		return actionsPerformed < 1;
	}
	case StageTwoState::Voting:
	{
		return actionsPerformed < Server::getNumberOfPlayers() - 1;
	}
	}

	return false;
}

uint8_t ServerStageTwo::getVisionCardsCount(uint8_t clarvoyancePoints)
{
	if (clarvoyancePoints > 6)
		return 3;

	if (clarvoyancePoints > 4)
		return 2;

	return 1;
}

bool ServerStageTwo::hasWon()
{
	uint8_t correctGuesses = std::count(psychicGuesses.begin(), psychicGuesses.end(), correctCircumstance);

	return correctGuesses >= (Server::getNumberOfPlayers()-1) / 2;
}

void ServerStageTwo::signalGameStateChange()
{
	for (auto& psychic : psychicClients)
	{
		auto stage = std::dynamic_pointer_cast<PsychicStageTwo>(psychic->getCurrentStage());
		if (stage)
		{
			stage->changeGameState(gameState);
		}
	}

	auto stage = std::dynamic_pointer_cast<GhostStageTwo>(ghostClient->getCurrentStage());
	if (stage)
	{
		stage->changeGameState(gameState);
	}
}

void ServerStageTwo::giveResults()
{
	// game over
	gameOver = true;

	bool result = hasWon();

	for (auto& client : psychicClients)
	{
		auto stage = client->getCurrentStage();
		if (stage)
			stage->setGameOver(result);
	}

	auto stage = ghostClient->getCurrentStage();
	if (stage)
		stage->setGameOver(result);
}
