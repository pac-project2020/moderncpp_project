#include "pch.h"

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include "../Mysterium/Button.cpp"

class ButtonTest
{
public:
	static bool testConstructorOne()
	{
		float height = 30;
		float width = 30;
		Button button(width, height);
		if (height == button.getDimension().y && width == button.getDimension().x
			&& button.getState() == 0)
		{
			return true;
		}
		return false;
	}
	static bool testConstructorTwo()
	{
		float x = 10;
		float y = 10;
		float height = 30;
		float width = 30;
		Button button(x, y, width, height);
		if (height == button.getDimension().y && width == button.getDimension().x &&
			x == button.getPosition().x && y == button.getPosition().y
			&& button.getState() == 0)
		{
			return true;
		}
		return false;
	}
	static bool testConstructorThree()
	{
		float x = 10;
		float y = 10;
		sf::Texture* texture = new sf::Texture();
		Button button(x, y, texture);
		if (&button.getTexture() == texture &&
			x == button.getPosition().x && y == button.getPosition().y
			&& button.getState() == 0)
		{
			return true;
		}
		return false;
	}
	static bool mouseTest()
	{
		float x = 0;
		float y = 0;
		float height = 30;
		float width = 30;
		Button button(x, y, width, height);
		sf::Vector2i position(15, 15);
		button.update(position);
		if (button.getState() == 1)
		{
			return true;
		}
		return false;
	}
};

TEST(ButtonTest, ConstructorTest)
{
	EXPECT_TRUE(ButtonTest::testConstructorOne());
	EXPECT_TRUE(ButtonTest::testConstructorTwo());
	EXPECT_TRUE(ButtonTest::testConstructorThree());
}

TEST(ButtonTest, MouseTest)
{
	EXPECT_TRUE(ButtonTest::mouseTest());
}