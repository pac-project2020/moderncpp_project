#include "pch.h"

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include "../Mysterium/Checkpoint.cpp"

class CheckpointTest
{
public:
	static bool testConstructorOne()
	{
		int x = 10;
		int y = 10;
		sf::Texture texture;
		Checkpoint checkpoint(x, y, texture);
		if (x != checkpoint.getPosition().x || y != checkpoint.getPosition().y)
		{
			return false;
		}
		if (&checkpoint.getTexture() != &texture)
		{
			return false;
		}
		return true;
	}
	static bool testConstructorTwo()
	{
		int x = 10;
		int y = 10;
		sf::Texture texture;
		Card card(texture, "name");
		std::shared_ptr<Card> card_ptr = std::make_shared<Card>(card);
		std::vector<std::shared_ptr<Card>> cards;
		cards.push_back(card_ptr);
		Checkpoint checkpoint(x, y, texture, cards);
		if (x != checkpoint.getPosition().x || y != checkpoint.getPosition().y)
		{
			return false;
		}
		if (&checkpoint.getTexture() != &texture)
		{
			return false;
		}
		if (checkpoint.getCards() != cards)
		{
			return false;
		}
		return true;
	}
	static bool insertTokenTest()
	{
		int checkpointX = 10;
		int checkpointY = 10;
		sf::Texture texture;
		Color color = Color::Blue;
		Token token(texture, color);
		std::shared_ptr<Token> token_ptr = std::make_shared<Token>(token);
		Checkpoint checkpoint(checkpointX, checkpointY, texture);
		checkpoint.insertToken(token_ptr);
		sf::Vector2i offset(10, 10);
		int padding = 40;
		int tokenX = checkpointX + offset.x + padding * checkpoint.getTokens().size();
		int tokenY = checkpointY + offset.y;
		if (tokenX == token_ptr.get()->getPosition().x && tokenY == token_ptr.get()->getPosition().y)
		{
			return 1;
		}
		return 0;
	}
	static bool removeTokenTest()
	{
		int checkpointX = 10;
		int checkpointY = 10;
		sf::Texture texture;
		Color color = Color::Blue;
		Token token(texture, color);
		std::shared_ptr<Token> token_ptr = std::make_shared<Token>(token);
		Checkpoint checkpoint(checkpointX, checkpointY, texture);
		checkpoint.insertToken(token_ptr);
		checkpoint.removeToken(Color::Blue);
		if (checkpoint.getTokens().empty())
		{
			return 1;
		}
		return 0;
	}
};

TEST(CheckpointTest, ConstructorTest) {
	EXPECT_TRUE(CheckpointTest::testConstructorOne());
	EXPECT_TRUE(CheckpointTest::testConstructorTwo());
}

TEST(CheckpointTest, insertTokenTest)
{
	EXPECT_TRUE(CheckpointTest::insertTokenTest());
}

TEST(CheckpointTest, removeTokenTest)
{
	EXPECT_TRUE(CheckpointTest::removeTokenTest());
}