#include "pch.h"

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include "../Mysterium/Tile.cpp"

TEST(TileTest, ConstructorTest) {
	sf::Texture texture;
	Tile tile(texture);
	EXPECT_EQ(&tile.getTexture(), &texture);
}