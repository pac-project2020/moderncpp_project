#include "pch.h"

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include "../Mysterium/Card.cpp"

class CardTest
{
public:
	static bool testConstructorOne()
	{
		sf::Texture texture;
		std::string cardName = "card";
		Card card(texture, cardName);
		if (&card.Tile::getTexture() == &texture && card.getName() == cardName)
		{
			return true;
		}
		return false;
	}
	static bool testConstructorTwo()
	{
		uint8_t x = 0;
		uint8_t y = 0;
		sf::Texture texture;
		std::string cardName = "card";
		Card card(x, y, texture, cardName);
		if (&card.Tile::getTexture() == &texture && card.getName() == cardName &&
			card.Tile::getPosition().x == x  && card.Tile::getPosition().y == y)
		{
			return true;
		}
		return false;
	}
	static bool insertTokenTest()
	{
		sf::Vector2i offset(10, 10);
		int padding = 40;
		sf::Texture texture;
		std::string cardName = "card";
		Card card(texture, cardName);
		Token token(texture, Color::Blue);
		std::shared_ptr<Token> token_ptr = std::make_shared<Token>(token);
		card.insertToken(token_ptr);
		int tokenX = card.Tile::getPosition().x + offset.x + card.getTokensVector().size() %2 * padding;
		int tokenY = card.Tile::getPosition().y + offset.y + card.getTokensVector().size() /2 * padding;
		if (token_ptr.get()->getPosition().x == tokenX && token_ptr.get()->getPosition().y == tokenY)
		{
			return true;
		}
		return false;
	}
	static bool containsTokenTest()
	{
		sf::Texture texture;
		std::string cardName = "card";
		Card card(texture, cardName);
		Token token(texture, Color::Blue);
		std::shared_ptr<Token> token_ptr = std::make_shared<Token>(token);
		card.insertToken(token_ptr);
		if (card.containsToken(Color::Blue) && !card.containsToken(Color::Red))
		{
			return true;
		}
		return false;
	}
	static bool removeTokenTest()
	{
		sf::Texture texture;
		std::string cardName = "card";
		Card card(texture, cardName);
		Token token1(texture, Color::Blue);
		Token token2(texture, Color::Red);
		std::shared_ptr<Token> token_ptr1 = std::make_shared<Token>(token1);
		std::shared_ptr<Token> token_ptr2 = std::make_shared<Token>(token2);
		card.insertToken(token_ptr1);
		card.insertToken(token_ptr2);
		card.removeToken(Color::Blue);
		if (card.containsToken(Color::Red) && !card.containsToken(Color::Blue))
		{
			return true;
		}
		return false;
	}
};

TEST(CardTest, ConstructorTest) {
	EXPECT_TRUE(CardTest::testConstructorOne());
	EXPECT_TRUE(CardTest::testConstructorTwo());
}

TEST(CardTest, insertTokenTest)
{
	EXPECT_TRUE(CardTest::insertTokenTest());
}

TEST(CardTest, ContainsTokenTest)
{
	EXPECT_TRUE(CardTest::containsTokenTest());
}

TEST(CardTest, RemoveTokenTest)
{
	EXPECT_TRUE(CardTest::removeTokenTest());
}