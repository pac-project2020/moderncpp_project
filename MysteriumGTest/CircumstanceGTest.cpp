#include "pch.h"

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include "../Mysterium/Circumstance.cpp"

class CircumstanceTest
{
public:
	static bool testConstructor()
	{
		std::shared_ptr<Tile> characterCard;
		std::shared_ptr<Tile> locationCard;
		std::shared_ptr<Tile> objectCard;
		Circumstance circumstance(characterCard, locationCard, objectCard);
		if (circumstance.getCharacterCard() == characterCard &&
			circumstance.getLocationCard() == locationCard &&
			circumstance.getObjectCard() == objectCard &&
			circumstance.getFaceDown() == 0)
		{
			return true;
		}
		return false;
	}
	static bool flipCardsTest()
	{
		std::shared_ptr<Tile> characterCard;
		std::shared_ptr<Tile> locationCard;
		std::shared_ptr<Tile> objectCard;
		Circumstance circumstance(characterCard, locationCard, objectCard);
		circumstance.flipNextCard();
		if (circumstance.getFaceDown() != 1 || &circumstance.getCharacterCard().get()->getTexture() != &Card::getBackTexture())
		{
			return 0;
		}
		circumstance.flipNextCard();
		if (circumstance.getFaceDown() != 2 || &circumstance.getLocationCard().get()->getTexture() != &Card::getBackTexture())
		{
			return 0;
		}
		circumstance.flipNextCard();
		if (circumstance.getFaceDown() != 3 || &circumstance.getObjectCard().get()->getTexture() != &Card::getBackTexture())
		{
			return 0;
		}
		return 1;
	}
	static bool initializeAsButtonTest()
	{
		std::shared_ptr<Tile> characterCard;
		std::shared_ptr<Tile> locationCard;
		std::shared_ptr<Tile> objectCard;
		characterCard.get()->setPosition(10, 10);
		locationCard.get()->setPosition(20, 20);
		objectCard.get()->setPosition(30, 30);
		Circumstance circumstance(characterCard, locationCard, objectCard);
		circumstance.initializeAsButton();
		if (circumstance.getPosition() != characterCard.get()->getPosition())
		{
			return 0;
		}
		int width = (objectCard.get()->getPosition().x + objectCard.get()->getDimension().x) - characterCard.get()->getPosition().x;
		int height = (objectCard.get()->getPosition().y + objectCard.get()->getDimension().y) - characterCard.get()->getPosition().y;
		if (circumstance.getDimension().x != width || circumstance.getDimension().y != height)
		{
			return 0;
		}
		return true;
	}
};

TEST(CircumstanceTest, ConstructorTest)
{
	EXPECT_TRUE(CircumstanceTest::testConstructor());
}

TEST(CircumstanceTest, FlipCardsTest)
{
	EXPECT_TRUE(CircumstanceTest::flipCardsTest());
}

TEST(CircumstanceTest, InitializeCardsAsButtonTest)
{
	EXPECT_TRUE(CircumstanceTest::initializeAsButtonTest());
}