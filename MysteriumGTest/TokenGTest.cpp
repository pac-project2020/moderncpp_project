#include "pch.h"

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include "../Mysterium/Token.cpp"

TEST(TokenTest, ConstructorTest) {
	sf::Texture texture;
	Color color = Color::Blue;
	Token token(texture, color);
	EXPECT_TRUE(token.getColor() == color);
	EXPECT_TRUE(&token.getTexture() == &texture);
}