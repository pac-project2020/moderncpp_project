#pragma once

#ifdef LOGGING_EXPORTS
#define LOGGER __declspec(dllexport)
#else
#define LOGGER __declspec(dllexport)
#endif

#include <chrono>
#include <iomanip>
#include <fstream>

class LOGGER Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};

public:
	template<class ... Args>
	static void Log(Level level, Args&& ... params)
	{
        #pragma warning(disable : 4996)
		auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		std::ofstream outputFile;
		outputFile.open("../Logging/logger.txt", std::ios::out | std::ios::app);
		switch (level)
		{
		case::Logger::Level::Error:
			outputFile << "[" << std::put_time(gmtime(&now), "%F %T") << "] [ERROR] ";
			break;
		case::Logger::Level::Info:
			outputFile << "[" << std::put_time(gmtime(&now), "%F %T") << "] [INFO] ";
			break;
		case::Logger::Level::Warning:
			outputFile << "[" << std::put_time(gmtime(&now), "%F %T") << "] [WARNING] ";
			break;
		default:
			break;

		}
	
		((outputFile << ' ' << std::forward<Args>(params)), ...);
		outputFile << '\n';
		outputFile.close();
	}
#pragma warning(default : 4996)
};

template<class ... Args>
void logi(Args&& ... params)
{
	Logger::Log(Logger::Level::Info, std::forward<Args>(params)...);
}




