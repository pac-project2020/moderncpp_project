#include "pch.h"
#include "CppUnitTest.h"
#include <sstream>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	/*TEST_CLASS(PlayersBoardTests)
	{
	public:
		TEST_METHOD(Constructor)
		{
			PlayersBoard playersboard(0, 0, 0);
			Assert::IsTrue(playersboard.getNumberOfPlayers() == 0);
			Assert::IsTrue(playersboard.getNumberClairvoyantsWhoGuessed() == 0);
			Assert::IsTrue(playersboard.showClock() == 0);
		}
		TEST_METHOD(actualizeClockTest)
		{
			PlayersBoard playersboard(0, 0, 0);
			playersboard.actualizeClock();
			Assert::IsTrue(playersboard.showClock() == 1);
		}
		TEST_METHOD(ClairvoyantsWhoGuessedTest)
		{
			PlayersBoard playersboard(0, 0, 0);
			playersboard.increaseNumberWhoGuessed();
			Assert::IsTrue(playersboard.getNumberClairvoyantsWhoGuessed() == 1);
		}
	};
	TEST_CLASS(CharacterCardTests)
	{
	public:
		TEST_METHOD(Constructor)
		{
			CharacterCard charactercard("name", 1);
			Assert::IsTrue(charactercard.getCardName() == "name");
			Assert::IsTrue(charactercard.getCardNumber() == 1);
		}
		TEST_METHOD(OutputStreamTest)
		{
			CharacterCard charactercard("name", 1);
			std::stringstream stringstream;
			stringstream << charactercard;
			std::string stringstream_str = stringstream.str();
			stringstream.clear();
			Assert::AreEqual(stringstream_str, std::string("name"));
		}
		TEST_METHOD(InputStreamTest)
		{
			CharacterCard charactercard;
			std::stringstream stringstream;
			stringstream << "name 1";
			stringstream >> charactercard;
			stringstream.clear();
			Assert::AreEqual(charactercard.getCardName(), std::string("name"));
			Assert::AreEqual(charactercard.getCardNumber(), 1);
		}
	};
	TEST_CLASS(LocationCardTests)
	{
	public:
		TEST_METHOD(Constructor)
		{
			LocationCard locationcard("name", 1);
			Assert::IsTrue(locationcard.getCardName() == "name");
			Assert::IsTrue(locationcard.getCardNumber() == 1);
		}
		TEST_METHOD(OutputStreamTest)
		{
			LocationCard locationcard("name", 1);
			std::stringstream stringstream;
			stringstream << locationcard;
			std::string stringstream_str = stringstream.str();
			stringstream.clear();
			Assert::AreEqual(stringstream_str, std::string("name"));
		}
		TEST_METHOD(InputStreamTest)
		{
			LocationCard locationcard;
			std::stringstream stringstream;
			stringstream << "name 1";
			stringstream >> locationcard;
			stringstream.clear();
			Assert::AreEqual(locationcard.getCardName(), std::string("name"));
			Assert::AreEqual(locationcard.getCardNumber(), 1);
		}
	};
	TEST_CLASS(ObjectCardTests)
	{
	public:
		TEST_METHOD(Constructor)
		{
			ObjectCard objectcard("name", 1);
			Assert::IsTrue(objectcard.getCardName() == "name");
			Assert::IsTrue(objectcard.getCardNumber() == 1);
		}
		TEST_METHOD(OutputStreamTest)
		{
			ObjectCard objectcard("name", 1);
			std::stringstream stringstream;
			stringstream << objectcard;
			std::string stringstream_str = stringstream.str();
			stringstream.clear();
			Assert::AreEqual(stringstream_str, std::string("name"));
		}
		TEST_METHOD(InputStreamTest)
		{
			ObjectCard objectcard;
			std::stringstream stringstream;
			stringstream << "name 1";
			stringstream >> objectcard;
			stringstream.clear();
			Assert::AreEqual(objectcard.getCardName(), std::string("name"));
			Assert::AreEqual(objectcard.getCardNumber(), 1);
		}
	};
	TEST_CLASS(VisionCardTests)
	{
	public:
		TEST_METHOD(Constructor)
		{
			VisionCard visioncard("name");
			Assert::IsTrue(visioncard.getCardName() == "name");
		}
		TEST_METHOD(OutputStreamTest)
		{
			VisionCard visioncard("name");
			std::stringstream stringstream;
			stringstream << visioncard;
			std::string stringstream_str = stringstream.str();
			stringstream.clear();
			Assert::AreEqual(stringstream_str, std::string("name"));
		}
		TEST_METHOD(InputStreamTest)
		{
			VisionCard visioncard;
			std::stringstream stringstream;
			stringstream << "name";
			stringstream >> visioncard;
			stringstream.clear();
			Assert::AreEqual(visioncard.getCardName(), std::string("name"));
		}
	};
	TEST_CLASS(CircumstanceTests)
	{
		TEST_METHOD(Constructor)
		{
			CharacterCard charactercard("character", 1);
			LocationCard locationcard("location", 2);
			ObjectCard objectcard("object", 3);
			Circumstance circumstance(charactercard, locationcard, objectcard);
			Assert::AreEqual(circumstance.getCharacaterCard().getCardName(), charactercard.getCardName());
			Assert::AreEqual(circumstance.getCharacaterCard().getCardNumber(), charactercard.getCardNumber());
			Assert::AreEqual(circumstance.getLocationCard().getCardName(), locationcard.getCardName());
			Assert::AreEqual(circumstance.getLocationCard().getCardNumber(), locationcard.getCardNumber());
			Assert::AreEqual(circumstance.getObjectCard().getCardName(), objectcard.getCardName());
			Assert::AreEqual(circumstance.getObjectCard().getCardNumber(), objectcard.getCardNumber());
		}
		TEST_METHOD(OutputStreamTest)
		{
			CharacterCard charactercard("character", 1);
			LocationCard locationcard("location", 2);
			ObjectCard objectcard("object", 3);
			Circumstance circumstance(charactercard, locationcard, objectcard);
			std::stringstream stringstream;
			stringstream << circumstance;
			std::string result = "Character: character\nLocation: location\nObject: object\n";
			std::string stringstream_str = stringstream.str();
			stringstream.clear();
			Assert::AreEqual(stringstream_str, result);
		}
		TEST_METHOD(InputStreamTest)
		{
			Circumstance circumstance;
			std::string result = "character 1 location 2 object 3";
			std::stringstream stringstream;
			stringstream << result;
			stringstream >> circumstance;
			stringstream.clear();
			Assert::AreEqual(circumstance.getCharacaterCard().getCardName(), std::string("character"));
			Assert::AreEqual(circumstance.getCharacaterCard().getCardNumber(), 1);
			Assert::AreEqual(circumstance.getLocationCard().getCardName(), std::string("location"));
			Assert::AreEqual(circumstance.getLocationCard().getCardNumber(), 2);
			Assert::AreEqual(circumstance.getObjectCard().getCardName(), std::string("object"));
			Assert::AreEqual(circumstance.getObjectCard().getCardNumber(), 3);

		}
	};*/
}